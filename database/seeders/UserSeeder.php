<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Administrator',
                'email' => 'admin@infoberitabali.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make('iNf0ber1t@b4Li'),
                'role' =>'admin',
                'active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        foreach ($data as $key => $value) {
            $cek = DB::table('user')->where('email','=',$value['email'])->first();
            if (!$cek) {
                DB::table('user')->insert($value);
            }
        }
    }
}
