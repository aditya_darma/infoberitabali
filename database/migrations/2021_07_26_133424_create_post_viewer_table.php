<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostViewerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_viewer', function (Blueprint $table) {
            $table->id();
            $table->foreignId('post_id')->constrained('post')->onUpdate('cascade')->onDelete('cascade');
            $table->ipAddress('ip');
            $table->string('user_agent');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_viewer');
    }
}
