<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function (Blueprint $table) {
            $table->id();
            $table->enum('type_post',['news','article']);
            $table->foreignId('category_post_id')->constrained('category_post')->onUpdate('cascade');
            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('content');
            $table->string('image');
            $table->string('keyword')->nullable();
            $table->boolean('highland')->default(false);
            $table->boolean('comment_reply')->default(false);
            $table->enum('status',['pending','publish'])->default('pending');
            $table->foreignId('user_id')->constrained('user')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post');
    }
}
