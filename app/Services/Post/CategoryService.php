<?php

namespace App\Services\Post;

use App\Repositories\CategoryPostRepository;
use App\Traits\ExceptionCustom;
use App\Traits\ResultService;

class CategoryService {
    use ResultService;
    use ExceptionCustom;
    
    protected $categoryPost;

    public function __construct(CategoryPostRepository $categoryPost)
    {
        $this->categoryPost = $categoryPost;
    }

    /**
     * List all data category post
     *
     * @return \App\Traits\ResultService
     */
    public function listAll()
    {
        $result = $this->categoryPost->getAll();

        return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('List kategori artikel')
                        ->setCode(200);
    }

    /**
     * List all data category post
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function findId(int $id)
    {
        try {
            $result = $this->categoryPost->findById($id);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data kategori artikel')
                        ->setCode(201);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Store data category post
     *
     * @param array $array
     * @return \App\Traits\ResultService
     */
    public function store(array $data)
    {
        try {
            $result = $this->categoryPost->store([
                'name' => $data['name'],
                'active' => $data['active']
            ]);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data berhasil ditambahkan')
                        ->setCode(201);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Update data category post 
     *
     * @param int $id
     * @param array $array
     * @return \App\Traits\ResultService
     */
    public function update(int $id, array $data)
    {
        try {
            $category = $this->categoryPost->findById($id);

            $result = $this->categoryPost->update($category, [
                'name' => $data['name'],
                'active' => $data['active']
            ]);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data berhasil dirubah')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Delete data category post
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function delete(int $id)
    {
        try {
            $category = $this->categoryPost->findById($id);
            $result = $this->categoryPost->delete($category);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data berhasil dihapus')
                        ->setCode(201);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
}