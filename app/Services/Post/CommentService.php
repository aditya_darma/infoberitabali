<?php

namespace App\Services\Post;

use App\Repositories\PostRepository;
use App\Repositories\CategoryPostRepository;
use App\Repositories\PostCommentRepository;
use App\Repositories\PostViewerRepository;
use App\Traits\ExceptionCustom;
use App\Traits\ResultService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class CommentService {
    use ResultService;
    use ExceptionCustom;

    protected $postCommentRepository;

    public function __construct(
        PostCommentRepository $postCommentRepository
    )
    {
        $this->postCommentRepository = $postCommentRepository;
    }

    /**
     * Get data comment latest 
     *
     * @return \App\Traits\ResultService
     */
    public function listAllCommentLatest()
    {
        $result = $this->postCommentRepository->getAllLatest();

        return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('List all comment')
                        ->setCode(200);
    }

    /**
     * Get data comment latest by id post
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function listCommentLatestByIdPost(int $id)
    {
        $result = $this->postCommentRepository->getLatestByIdPost($id);

        return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('List comment by id post')
                        ->setCode(200);
    }

    /**
     * Get data comment by id
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function getCommentById(int $id)
    {
        $result = $this->postCommentRepository->findById($id);

        return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data comment by id')
                        ->setCode(200);
    }

    /**
     * Store post comment
     *
     * @param array $data
     * @return \App\Traits\ResultService
     */
    public function storePostComment(array $data)
    {
        try {
            $result = $this->postCommentRepository->create([
                'post_id' => $data['id'],
                'comment' => $data['comment'],
                'active' => 1,
                'user_id' => Auth::user()->id
            ]);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data berhasil ditambahkan')
                        ->setCode(201);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Update post comment
     *
     * @param array $data
     * @param int $id
     * @return \App\Traits\ResultService
     */
    public function updatePostComment(array $data, int $id)
    {
        try {
            $comment = $this->postCommentRepository->findById($id);
            $result = $this->postCommentRepository->update($comment, [
                'comment' => $data['comment'],
            ]);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data berhasil diupdate')
                        ->setCode(201);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get comment post by id post
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function getPostComment(int $id)
    {
        $comment = $this->postCommentRepository->getByIdPostLatestCursorPaginate($id,5);

        $data['view'] = view('component.comment-post', ['comment' => $comment])->render();
        $data['link'] = $comment->nextPageUrl();

        return $this->setSuccess(true)
                    ->setResult($data)
                    ->setMessage('Data komentar post')
                    ->setCode(200);
    }

    /**
     * Set comment show
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function showComment(int $id)
    {
        try {
            $comment = $this->postCommentRepository->findById($id);
            $this->postCommentRepository->setShowComment($comment);

            return $this->setSuccess(true)
                        ->setMessage('Komentar berhasil ditampilkan')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Set comment show
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function hideComment(int $id)
    {
        try {
            $comment = $this->postCommentRepository->findById($id);
            $this->postCommentRepository->setHideComment($comment);

            return $this->setSuccess(true)
                        ->setMessage('Komentar berhasil disembunyikan')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Delete data comment
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function deleteComment(int $id)
    {
        try {
            $comment = $this->postCommentRepository->findById($id);
            $this->postCommentRepository->delete($comment);

            return $this->setSuccess(true)
                        ->setMessage('Komentar berhasil dihapus')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
}