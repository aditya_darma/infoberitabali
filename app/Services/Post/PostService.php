<?php

namespace App\Services\Post;

use App\Repositories\PostRepository;
use App\Repositories\CategoryPostRepository;
use App\Repositories\PostCommentRepository;
use App\Repositories\PostViewerRepository;
use App\Traits\ExceptionCustom;
use App\Traits\ResultService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PostService {
    use ResultService;
    use ExceptionCustom;

    protected $categoryPost;
    protected $post;
    protected $postComment;
    protected $postViewer;

    public function __construct(
        CategoryPostRepository $categoryPost,
        PostRepository $post,
        PostCommentRepository $postComment,
        PostViewerRepository $postViewer
    )
    {
        $this->categoryPost = $categoryPost;
        $this->post = $post;
        $this->postComment = $postComment;
        $this->postViewer = $postViewer;
    }

    /**
     * Get data to create post
     *
     * @return \App\Traits\ResultService
     */
    public function listAll()
    {
        $result = $this->post->getAllLatest();

        return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('List artikel')
                        ->setCode(200);
    }

    /**
     * Get data to create post latest
     *
     * @return \App\Traits\ResultService
     */
    public function listAllLatest()
    {
        $result = $this->post->getAllLatest();

        return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('List artikel')
                        ->setCode(200);
    }

    /**
     * Get data to create post
     *
     * @return \App\Traits\ResultService
     */
    public function create()
    {
        $result['category'] = $this->categoryPost->getAllActive();
        $result['type'] = typePost();
        $result['status'] = statusPost();

        return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Buat artikel')
                        ->setCode(200);
    }

    /**
     * Store data to post
     *
     * @param array $array
     * @return \App\Traits\ResultService
     */
    public function store(array $data)
    {
        try {
            $file = $data['image'];
            $name = Str::slug($data['title'],'-').'-'. date('YmdHis') .'.'. $file->getClientOriginalExtension();

            $image = Image::make($file);
            $image->fit(1350, 900, function ($constraint) {
                $constraint->aspectRatio();
            });

            if(!is_dir(public_path('content/photo/post/'))) mkdir(public_path('content/photo/post/'), 0777, TRUE);
            $image->save(public_path('content/photo/post/'.$name));

            $result = $this->post->store([
                'type_post' => $data['type'],
                'category_post_id' => $data['category'],
                'title' => $data['title'],
                'content' => $data['content'],
                'image' => $name,
                'keyword' => $data['keyword'],
                'highland' => $data['highland'],
                'comment_reply' => $data['comment_reply'],
                'status' => $data['status'],
                'user_id' => Auth::user()->id
            ]);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data artikel berhasil dibuat')
                        ->setCode(201);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data to create post
     *
     * @return \App\Traits\ResultService
     */
    public function edit(int $id)
    {
        try {
            $result['post'] = $this->post->findById($id);
            $result['category'] = $this->categoryPost->getAllActive();
            $result['type'] = typePost();
            $result['status'] = statusPost();

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Edit artikel')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Update data post
     *
     * @param int $id
     * @param array $array
     * @return \App\Traits\ResultService
     */
    public function update(int $id, array $data)
    {
        try {
            $post = $this->post->findById($id);

            $params = [
                'type_post' => $data['type'],
                'category_post_id' => $data['category'],
                'title' => $data['title'],
                'content' => $data['content'],
                'keyword' => $data['keyword'],
                'highland' => $data['highland'],
                'comment_reply' => $data['comment_reply'],
                'status' => $data['status'],
                'user_id' => Auth::user()->id
            ];

            if ($data['image']) {
                $file = $data['image'];
                $name = Str::slug($data['title'],'-').'-'. date('YmdHis') .'.'. $file->getClientOriginalExtension();

                $image = Image::make($file);
                $image->fit(1350, 900, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $image->save(public_path('content/photo/post/'.$name));

                $params['image'] = $name;
            }

            $this->post->update($post, $params);

            return $this->setSuccess(true)
                        ->setMessage('Data artikel berhasil dirubah')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Delete data post
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function delete(int $id)
    {
        try {
            $post = $this->post->findById($id);
            $this->post->delete($post);

            return $this->setSuccess(true)
                        ->setMessage('Data berhasil dihapus')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post
     *
     * @param integer $id
     * @return \App\Traits\ResultService
     */
    public function getPost(int $id)
    {
        try {
            $result['post'] = $this->post->findById($id);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data Post')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get for home page
     *
     * @return \App\Traits\ResultService
     */
    public function homeWebsite()
    {
        try {
            $data['highland'] = $this->post->highlandLatestMonthRandomFirst();
            $data['news'] = $this->post->latesNewstMonth(4);
            $data['populars_article'] = $this->post->highViewerArticleLatestMonth(4);
            $data['latests_article'] = $this->post->latestArticle(3);

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data for home page')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get for home page
     *
     * @param string $slug
     * @return \App\Traits\ResultService
     */
    public function postDetailBySlug(string $slug)
    {
        try {
            $post = $this->post->findBySlug($slug);
            $data['post'] = $post;

            $this->postViewer->findOrCreate([
                'post_id' => $post->id,
                'ip' => request()->ip(),
                'user_agent' => request()->server('HTTP_USER_AGENT'),
                'date' => date('Y-m-d')
            ]);

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data for home page')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post news
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostNews(?string $search)
    {
        try {
            $data['title'] = 'News';
            $data['post'] = $this->post->cursorPaginateNews(10, $search);

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data news paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post news view
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostNewsView(?string $search)
    {
        try {
            $post = $this->post->cursorPaginateNews(10, $search);
            $data['view'] = view('component.list-post', ['post' => $post, 'column' => randomPostStyle()])->render();
            $data['link'] = $post->nextPageUrl();

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data news paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post article
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostArticle(?string $search)
    {
        try {
            $data['title'] = 'Article';
            $data['post'] = $this->post->cursorPaginateArticle(10,$search);

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data article paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post article view
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostArticleView(?string $search)
    {
        try {
            $post = $this->post->cursorPaginateArticle(10,$search);
            $data['view'] = view('component.list-post', ['post' => $post, 'column' => randomPostStyle()])->render();
            $data['link'] = $post->nextPageUrl();

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data news paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post article popular
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostArticlePopular(?string $search)
    {
        try {
            $data['title'] = 'Article';
            $data['post'] = $this->post->cursorPaginateArticlePopular(10,$search);

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data article paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post article popular view
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostArticlePopularView(?string $search)
    {
        try {
            $post = $this->post->cursorPaginateArticlePopular(10,$search);
            $data['view'] = view('component.list-post', ['post' => $post, 'column' => randomPostStyle()])->render();
            $data['link'] = $post->nextPageUrl();

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data news paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post highland
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostHighland(?string $search)
    {
        try {
            $data['title'] = 'Highlight';
            $data['post'] = $this->post->cursorPaginateHighland(10, $search);

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data highland paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post highland view
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostHighlandView(?string $search)
    {
        try {
            $post = $this->post->cursorPaginateHighland(10, $search);
            $data['view'] = view('component.list-post', ['post' => $post, 'column' => randomPostStyle()])->render();
            $data['link'] = $post->nextPageUrl();

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data highland paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post by category
     *
     * @param string $slug
     * @return \App\Traits\ResultService
     */
    public function getPostCategory(string $slug)
    {
        try {
            $category = $this->categoryPost->findBySlug($slug);

            $data['title'] = $category->name;
            $data['post'] = $this->post->cursorPaginateCategory($category->id, 10);

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data category paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post by category view
     *
     * @param string $slug
     * @return \App\Traits\ResultService
     */
    public function getPostCategoryView(string $slug)
    {
        try {
            $category = $this->categoryPost->findBySlug($slug);

            $post = $this->post->cursorPaginateCategory($category->id, 10);
            $data['view'] = view('component.list-post', ['post' => $post, 'column' => randomPostStyle()])->render();
            $data['link'] = $post->nextPageUrl();

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data category paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post search
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostSearch(?string $search)
    {
        try {
            $data['title'] = 'Search';
            $data['post'] = $this->post->cursorPaginateSearch(10, $search);

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data search paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Get data post search view
     *
     * @param string|null $search
     * @return \App\Traits\ResultService
     */
    public function getPostSearchView(?string $search)
    {
        try {
            $post = $this->post->cursorPaginateSearch(10, $search);
            $data['view'] = view('component.list-post', ['post' => $post, 'column' => randomPostStyle()])->render();
            $data['link'] = $post->nextPageUrl();

            return $this->setSuccess(true)
                        ->setResult($data)
                        ->setMessage('Data search paginate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
}
