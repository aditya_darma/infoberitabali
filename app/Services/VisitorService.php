<?php

namespace App\Services;

use App\Repositories\VisitorRepository;
use App\Traits\ExceptionCustom;
use App\Traits\ResultService;

class VisitorService
{
    use ResultService;
    use ExceptionCustom;

    protected $visitor;

    public function __construct(
        VisitorRepository $visitor
    )
    {
        $this->visitor = $visitor;
    }

    public function countVisitorByDate(array $data)
    {
        $result = $this->visitor->getCountVisitorGroupByDate([
            'date_start' => $data['date_start'],
            'date_end' => $data['date_end']
        ]);

        $date = [];
        $total = [];

        foreach ($result as $value) {
            $date[] = $value->date;
            $total[] = $value->total;
        }

        return $this->setSuccess(true)
                    ->setResult(['date' => $date, 'total' => $total])
                    ->setMessage('Data visitor')
                    ->setCode(200);
    }
}