<?php

namespace App\Services;

use App\Mail\MessageFromContactEmail;
use App\Traits\ExceptionCustom;
use App\Traits\ResultService;
use Illuminate\Support\Facades\Mail;

class NotificationService
{
    use ResultService;
    use ExceptionCustom;

    public function __construct(
        
    )
    {
        
    }

    /**
     * Send email contact us
     *
     * @param array $data
     * @return ResultService
     */
    public function sendEmailContact(array $data)
    {
        try {
            Mail::to(env('MAIL_FROM_ADDRESS'))->send(new MessageFromContactEmail($data));

            return $this->setSuccess(true)
                        ->setMessage('Email berhasil dikirim')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
}