<?php

namespace App\Services;

use App\Repositories\AdsRepository;
use App\Traits\ExceptionCustom;
use App\Traits\ResultService;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class AdsService
{
    use ResultService;
    use ExceptionCustom;

    protected $ads;

    public function __construct(
        AdsRepository $ads
    )
    {
        $this->ads = $ads;
    }

    public function findId(int $id)
    {
        try {
            $result['ads'] = $this->ads->findById($id);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data iklan berdasarkan ID')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    public function getAll()
    {
        try {
            $result = $this->ads->getAllLatest();

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Semua data iklan')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    public function create()
    {
        $result['type'] = typeAds();

        return $this->setSuccess(true)
                    ->setResult($result)
                    ->setMessage('Data untuk tambah iklan')
                    ->setCode(200);
    }

    public function store(array $data)
    {
        $file = $data['image'];
        $name = Str::slug($data['name'],'-').'-'. date('YmdHis') .'.'. $file->getClientOriginalExtension();
        $size = sizeAds()[$data['size_type']];
        
        $image = Image::make($file);
        $image->fit($size[0], $size[1], function ($constraint) {
            $constraint->aspectRatio();
        });
        
        if(!is_dir(public_path('content/photo/ads/'))) mkdir(public_path('content/photo/ads/'), 0777, TRUE);
        $image->save(public_path('content/photo/ads/'.$name));
        
        $result = $this->ads->create([
            'name' => $data['name'],
            'size_type' => $data['size_type'],
            'date_start' => $data['date_start'],
            'date_end' => $data['date_end'],
            'image' => $name,
            'link' => $data['link'],
            'active' => $data['status']
        ]);

        return $this->setSuccess(true)
                    ->setResult($result)
                    ->setMessage('Data Berhasil Ditambahkan')
                    ->setCode(201);
    }

    public function edit(int $id)
    {
        try {
            $result['ads'] = $this->ads->findOrFailById($id);
            $result['type'] = typeAds();

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data iklan berdasarkan ID')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    public function update(int $id, array $data)
    {
        try {
            $ads = $this->ads->findOrFailById($id);

            $result = $this->ads->update($ads, [
                'name' => $data['name'],
                'size_type' => $data['size_type'],
                'date_start' => $data['date_start'],
                'date_end' => $data['date_end'],
                'link' => $data['link'],
                'active' => $data['status']
            ]);

            // Upload image
            $file = $data['image'];
            if ($file) {
                $old_name = $ads->image;
                $new_name = Str::slug($data['name'],'-').'-'. date('YmdHis') .'.'. $file->getClientOriginalExtension();
                $size = sizeAds()[$data['size_type']];
                
                $image = Image::make($file);
                $image->fit($size[0], $size[1], function ($constraint) {
                    $constraint->aspectRatio();
                });

                $image->save(public_path('content/photo/ads/'.$new_name));

                $result = $this->ads->update($ads, [
                    'image' => $new_name,
                ]);

                unlink(public_path('content/photo/ads/'.$old_name));
            }

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data Berhasil Dirubah')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    public function delete(int $id)
    {
        try {
            $ads = $this->ads->findOrFailById($id);
            $name = $ads->image;

            $this->ads->delete($ads);
            
            unlink(public_path('content/photo/ads/'.$name));

            return $this->setSuccess(true)
                        ->setMessage('Data Berhasil Dihapus')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
}