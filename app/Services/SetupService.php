<?php

namespace App\Services;

use App\Repositories\SetupRepository;
use App\Traits\ExceptionCustom;
use App\Traits\ResultService;
use Illuminate\Support\Facades\Cache;

class SetupService
{
    use ResultService;
    use ExceptionCustom;

    protected $setup;

    public function __construct(
        SetupRepository $setup
    )
    {
        $this->setup = $setup;
    }

    public function aboutUs()
    {
        try {
            $result['setup'] = $this->setup->findByName('about_us');

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data setup about us')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    public function aboutUsUpdate(array $data)
    {
        try {
            $setup = $this->setup->findByName('about_us');
            $result = $this->setup->update($setup, [
                'value' => $data['content']
            ]);

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data about us diupdate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    public function footer()
    {
        try {
            $result['setup'] = $this->setup->getPluckValueNameAll();

            return $this->setSuccess(true)
                        ->setResult($result)
                        ->setMessage('Data stup about us')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
    
    public function footerUpdate(array $data)
    {
        try {
            foreach ($data['setup'] as $key => $value) {
                $setup = $this->setup->findByName($key);
                $result = $this->setup->update($setup, [
                    'value' => $value
                ]);
                
                Cache::forget($key);
            }
            
            return $this->setSuccess(true)
                        ->setResult(true)
                        ->setMessage('Data berhasil diupdate')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
}