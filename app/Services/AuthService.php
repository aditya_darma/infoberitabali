<?php

namespace App\Services;

use App\Mail\AccountVerificationEmail;
use App\Repositories\UserRepository;
use App\Traits\ExceptionCustom;
use App\Traits\ResultService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthService {
    use ResultService;
    use ExceptionCustom;

    protected $user;

    public function __construct(
        UserRepository $user
    )
    {
        $this->user = $user;
    }

    /**
     * Login admin
     *
     * @param array $data
     * @return boolean
     */
    public function loginAdmin(array $data)
    {
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'role' => 'admin', 'active' => 1], $data['remember'])) {
            return true;
        }
        
        return false; 
    }

    /**
     * Logout admin
     *
     * @return boolean
     */
    public function logoutAdmin()
    {
        if (Auth::user()->role == 'admin') {
            Auth::logout();

            return true;
        }

        return false;
    }

    /**
     * Register visitor
     *
     * @param array $data
     * @return ResultService
     */
    public function registerVisitor(array $data)
    {
        try {
            $user = $this->user->create([
                'name' => $data['name'],
                'email' => $data['email'],
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make($data['password']),
                'role' => 'visitor',
                'active' => 0
            ]);

            Mail::to($user->email)->send(new AccountVerificationEmail($user));

            return $this->setSuccess(true)
                        ->setResult(true)
                        ->setMessage('Akun berhasil dibuat, Silakan cek inbox atau spam pada email anda untuk aktivasi akun')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }

    /**
     * Login visitor
     *
     * @param array $data
     * @return ResultService
     */
    public function loginVisitor(array $data)
    {
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'role' => 'visitor'])) {
            if (!Auth::user()->active) {
                Auth::logout();
                return $this->setSuccess(false)
                            ->setResult(false)
                            ->setMessage('Akun anda belum aktif, silakan cek email untuk verifikasi')
                            ->setCode(200);
            }
            return $this->setSuccess(true)
                        ->setResult(true)
                        ->setMessage('Anda Berhasil Login')
                        ->setCode(200);
        }
        
        return $this->setSuccess(false)
                        ->setResult(false)
                        ->setMessage('Anda Gagal Login')
                        ->setCode(200);
    }

    /**
     * Logout visitor
     *
     * @return boolean
     */
    public function logoutVisitor()
    {
        Auth::logout();

        return true;
    }

    /**
     * Verification account
     *
     * @param array $data
     * @return ResultService
     */
    public function verification(array $data)
    {
        try {
            $user = $this->user->findByEmail(base64_decode($data['token']));
            $this->user->setActive($user, ['active' => 1]);

            return $this->setSuccess(true)
                        ->setResult(true)
                        ->setMessage('Akun berhasil di aktivasi')
                        ->setCode(200);
        } catch (\Exception $e) {
            return $this->exceptionResponse($e);
        }
    }
}