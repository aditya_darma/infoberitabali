<?php

namespace App\Repositories;

use App\Models\PostComment;

class PostCommentRepository {
    protected $model;

    public function __construct(
        PostComment $post
    )
    {
        $this->model = $post;
    }

    /**
     * Find by ID comment
     *
     * @param int $id
     * @return PostComment
     */
    public function findById(int $id)
    {
        return $this->model
                    ->findOrFail($id);
    }

    /**
     * Get all data comment
     *
     * @return PostComment
     */
    public function getAllLatest()
    {
        return $this->model
                    ->with(['post','user'])
                    ->latest()
                    ->get();
    }

    /**
     * Get all data comment
     *
     * @param integer $id
     * @return PostComment
     */
    public function getLatestByIdPost(int $id)
    {
        return $this->model
                    ->with(['post','user'])
                    ->where('post_id','=',$id)
                    ->latest()
                    ->get();
    }

    /**
     * Create data comment
     *
     * @param array $data
     * @return PostComment
     */
    public function create(array $data)
    {
        return $this->model
                    ->create($data);
    }

    /**
     * Update data comment
     *
     * @param array $data
     * @param PostComment $comment
     * @return bool
     */
    public function update(PostComment $comment,array $data)
    {
        return $comment->update($data);
    }

    /**
     * Data comment post by id post cursor paginate
     *
     * @param integer $id
     * @param integer $length
     * @return \Illuminate\Contracts\Pagination\CursorPaginator
     */
    public function getByIdPostLatestCursorPaginate(int $id, int $length)
    {
        return $this->model
                    ->with(['user'])
                    ->where('post_id','=',$id)
                    ->where('active','=',1)
                    ->latest()
                    ->cursorPaginate($length);
    }

    /**
     * Delete data model post comment
     *
     * @param PostComment $comment
     * @return bool
     */
    public function delete(PostComment $comment)
    {
        return $comment->delete();
    }

    /**
     * Set comment active true
     *
     * @param PostComment $comment
     * @return bool
     */
    public function setShowComment(PostComment $comment)
    {
        return $comment->update([
                            'active' => 1
                        ]);
    }

    /**
     * Set comment active false
     *
     * @param PostComment $comment
     * @return bool
     */
    public function setHideComment(PostComment $comment)
    {
        return $comment->update([
                            'active' => 0
                        ]);
    }


}