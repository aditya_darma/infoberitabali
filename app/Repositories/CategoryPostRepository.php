<?php

namespace App\Repositories;

use App\Models\CategoryPost;

class CategoryPostRepository {
    protected $model;

    public function __construct(
        CategoryPost $categoryPost
    )
    {
        $this->model = $categoryPost;
    }

    /**
     * Get all data category post
     *
     * @return \App\Models\CategoryPost
     */
    public function getAll()
    {
        return $this->model
                    ->get();
    }

    /**
     * Get all data active category post
     *
     * @return \App\Models\CategoryPost
     */
    public function getAllActive()
    {
        return $this->model
                    ->where('active','=',1)
                    ->get();
    }

    /**
     * Find data by ID category  post
     *
     * @param integer $id
     * @return \App\Models\CategoryPost
     */
    public function findById(int $id)
    {
        return $this->model
                    ->findOrFail($id);
    }

    /**
     * Find data by ID category  post
     *
     * @param string $slug
     * @return \App\Models\CategoryPost
     */
    public function findBySlug(string $slug)
    {
        return $this->model
                    ->where('slug','=',$slug)
                    ->first();
    }

    /**
     * Store data to model category post
     *
     * @param array $data
     * @return \App\Models\CategoryPost
     */
    public function store(array $data)
    {
        return $this->model
                    ->create($data);
    }

    /**
     * Update data to model category post
     *
     * @param \App\Models\CategoryPost $category
     * @param array $data
     * @return bool
     */
    public function update(CategoryPost $category, array $data)
    {
        return $category->update($data);
    }

    /**
     * Delete data model category post
     *
     * @param \App\Models\CategoryPost $category
     * @return bool
     */
    public function delete(CategoryPost $category)
    {
        return $category->delete();
    }
}