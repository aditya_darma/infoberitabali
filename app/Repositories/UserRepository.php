<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository {
    protected $model;
    
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * Get all data user
     *
     * @return \App\Models\User
     */
    public function getAll()
    {
        return $this->model
                    ->get();
    }

    /**
     * Find by email
     *
     * @return \App\Models\User
     */
    public function findByEmail(string $email)
    {
        return $this->model
                    ->where('email','=',$email)
                    ->firstOrFail();
    }

    /**
     * Store data user
     *
     * @param array $data
     * @return \App\Models\User
     */
    public function create(array $data)
    {
        return $this->model
                    ->create($data);
    }

    /**
     * Set active data user
     *
     * @param User $user
     * @param array $data
     * @return bool
     */
    public function setActive(User $user, array $data)
    {
        return $user->update($data);
    }
}