<?php

namespace App\Repositories;

use App\Models\Setup;

class SetupRepository {
    protected $model;

    public function __construct(
        Setup $setup
    )
    {
        $this->model = $setup;
    }

    /**
     * Find data by name  setup
     *
     * @param string $name
     * @return \App\Models\Setup
     */
    public function findByName(string $name)
    {
        return $this->model
                    ->where('name','=',$name)
                    ->firstOrFail();
    }

    /**
     * Get pluck value name all data 
     *
     * @return \App\Models\Setup
     */
    public function getPluckValueNameAll()
    {
        return $this->model
                    ->pluck('value','name');
    }

    /**
     * Store data to model setup
     *
     * @param \App\Models\Setup $setup
     * @param array $data
     * @return bool
     */

    public function update(Setup $setup, array $data)
    {
        return $setup->update($data);
    }
}