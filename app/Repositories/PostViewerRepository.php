<?php

namespace App\Repositories;

use App\Models\PostViewer;

class PostViewerRepository {
    protected $model;

    public function __construct(
        PostViewer $post
    )
    {
        $this->model = $post;
    }

    /**
     * Create data viewer
     *
     * @param array $data
     * @return PostViewer
     */
    public function findOrCreate(array $data)
    {
        return $this->model
                    ->firstOrCreate($data);
    }
}