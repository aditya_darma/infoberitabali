<?php

namespace App\Repositories;

use App\Models\Ads;

class AdsRepository {
    protected $model;

    public function __construct(
        Ads $ads
    )
    {
        $this->model = $ads;
    }

    public function getAll()
    {
        return $this->model
                    ->get();
    }

    public function getAllLatest()
    {
        return $this->model
                    ->latest()
                    ->get();
    }

    public function getAllActive()
    {
        return $this->model
                    ->where('active','=',1)
                    ->get();
    }

    public function getAllActiveLatest()
    {
        return $this->model
                    ->where('active','=',1)
                    ->latest()
                    ->get();
    }

    public function getAllActiveToday(string $date)
    {
        return $this->model
                    ->where('active','=',1)
                    ->where('date_start','<=',$date)
                    ->where('date_end','>=',$date)
                    ->get();
    }

    public function getAllSecondActiveTodayTake(string $date, int $take)
    {
        return $this->model
                    ->where('active','=',1)
                    ->where('date_start','<=',$date)
                    ->where('date_end','>=',$date)
                    ->where('size_type','=',2)
                    ->inRandomOrder()
                    ->take($take)
                    ->get();
    }

    public function getAllFirstActiveTodayFirst(string $date)
    {
        return $this->model
                    ->where('active','=',1)
                    ->where('date_start','<=',$date)
                    ->where('date_end','>=',$date)
                    ->where('size_type','=',1)
                    ->inRandomOrder()
                    ->first();
    }

    public function getAllSecondActiveTodayFirst(string $date)
    {
        return $this->model
                    ->where('active','=',1)
                    ->where('date_start','<=',$date)
                    ->where('date_end','>=',$date)
                    ->where('size_type','=',2)
                    ->inRandomOrder()
                    ->first();
    }

    public function findById(int $id)
    {
        return $this->model
                    ->find($id);
    }

    public function findOrFailById(int $id)
    {
        return $this->model
                    ->findOrFail($id);
    }

    public function create(array $data)
    {
        return $this->model
                    ->create($data);
    }

    public function update(Ads $ads, array $data)
    {
        return $ads->update($data);
    }

    public function delete(Ads $ads)
    {
        return $ads->delete();
    }
}