<?php

namespace App\Repositories;

use App\Models\Post;

class PostRepository {
    protected $model;

    public function __construct(
        Post $post
    )
    {
        $this->model = $post;
    }

    /**
     * Find data by ID  post
     *
     * @param integer $id
     * @return \App\Models\Post
     */
    public function findById(int $id)
    {
        return $this->model
                    ->findOrFail($id);
    }

    /**
     * Get data by slug
     *
     * @param string $slug
     * @return \App\Models\Post
     */
    public function findBySlug(string $slug)
    {
        return $this->model
                    ->with(['categoryPost','user','getCommentName'])
                    ->withCount('viewer')
                    ->withCount(['comment' => function($query) {
                        $query->where('active','=',1);
                    }])
                    ->where('slug','=',$slug)
                    ->first();
    }

    /**
     * Get all data post news latest
     *
     * @param int $length
     * @param string|null $search
     * @return \App\Models\Post
     */
    public function cursorPaginateNews(int $length, ?string $search)
    {
        return $this->model
                    ->with(['categoryPost','user','getCommentName'])
                    ->withCount('viewer')
                    ->withCount(['comment' => function($query) {
                        $query->where('active','=',1);
                    }])
                    ->where('type_post','=','news')
                    ->when($search, function($q) use($search){
                        return $q->where('title', 'like', '%'.$search.'%');
                    })
                    ->orderBy('id','desc')
                    ->cursorPaginate($length)
                    ->withQueryString();
    }

    /**
     * Get all data post article latest
     *
     * @param int $length
     * @param string|null $search
     * @return \App\Models\Post
     */
    public function cursorPaginateArticle(int $length, ?string $search)
    {
        return $this->model
                    ->with(['categoryPost','user','getCommentName'])
                    ->withCount('viewer')
                    ->withCount(['comment' => function($query) {
                        $query->where('active','=',1);
                    }])
                    ->where('type_post','=','article')
                    ->when($search, function($q) use($search){
                        return $q->where('title', 'like', '%'.$search.'%');
                    })
                    ->orderBy('id','desc')
                    ->cursorPaginate($length)
                    ->withQueryString();
    }

    /**
     * Get all data post article popular
     *
     * @param int $length
     * @param string|null $search
     * @return \App\Models\Post
     */
    public function cursorPaginateArticlePopular(int $length, ?string $search)
    {
        return $this->model
                    ->with(['categoryPost','user','getCommentName'])
                    ->withCount('viewer')
                    ->withCount(['comment' => function($query) {
                        $query->where('active','=',1);
                    }])
                    ->where('type_post','=','article')
                    ->when($search, function($q) use($search){
                        return $q->where('title', 'like', '%'.$search.'%');
                    })
                    ->orderBy('viewer_count','desc')
                    ->orderBy('updated_at','desc')
                    ->cursorPaginate($length)
                    ->withQueryString();
    }

    /**
     * Get all data post hightland latest
     *
     * @param int $length
     * @param string|null $search
     * @return \App\Models\Post
     */
    public function cursorPaginateHighland(int $length, ?string $search)
    {
        return $this->model
                    ->with(['categoryPost','user','getCommentName'])
                    ->withCount('viewer')
                    ->withCount(['comment' => function($query) {
                        $query->where('active','=',1);
                    }])
                    ->where('highland','=',1)
                    ->when($search, function($q) use($search){
                        return $q->where('title', 'like', '%'.$search.'%');
                    })
                    ->orderBy('id','desc')
                    ->cursorPaginate($length)
                    ->withQueryString();
    }

    /**
     * Get all data post hightland latest
     *
     * @param int $length
     * @param string|null $search
     * @return \App\Models\Post
     */
    public function cursorPaginateSearch(int $length, ?string $search)
    {
        return $this->model
                    ->with(['categoryPost','user','getCommentName'])
                    ->withCount('viewer')
                    ->withCount(['comment' => function($query) {
                        $query->where('active','=',1);
                    }])
                    ->when($search, function($q) use($search){
                        return $q->where('title', 'like',  '%'.$search.'%');
                    })
                    ->orderBy('id','desc')
                    ->cursorPaginate($length)
                    ->withQueryString();
    }

    /**
     * Get all data post by category latest
     *
     * @param int $category_id
     * @param int $length
     * @return \App\Models\Post
     */
    public function cursorPaginateCategory(int $category_id, int $length)
    {
        return $this->model
                    ->with(['categoryPost','user','getCommentName'])
                    ->withCount('viewer')
                    ->withCount(['comment' => function($query) {
                        $query->where('active','=',1);
                    }])
                    ->where('category_post_id','=',$category_id)
                    ->when(request()->search, function($q){
                        return $q->where('title', 'like', request()->search.'%');
                    })
                    ->orderBy('id','desc')
                    ->cursorPaginate($length)
                    ->withQueryString();
    }

    /**
     * Get all data post latest
     *
     * @return \App\Models\Post
     */
    public function getAllLatest()
    {
        return $this->model
                    ->with(['categoryPost','user','getCommentName'])
                    ->latest('id')
                    ->get();
    }

     /**
     * Store data to model post
     *
     * @param array $data
     * @return \App\Models\Post
     */
    public function store(array $data)
    {
        return $this->model
                    ->create($data);
    }

    /**
     * Update data to model post
     *
     * @param \App\Models\Post $post
     * @param array $data
     * @return bool
     */
    public function update(Post $post, array $data)
    {
        return $post->update($data);
    }

    /**
     * Delete data model post
     *
     * @param \App\Models\Post $post
     * @return bool
     */
    public function delete(Post $post)
    {
        return $post->delete();
    }

    /**
     * Get data post highland random
     *
     * @return \App\Models\Post
     */
    public function highlandLatestMonthRandomFirst()
    {
        return $this->model
                    ->where('highland','=',1)
                    ->where('status', '=', Post::STATUS_PUBLISH)
                    ->latest('id')
                    ->first();
    }

    /**
     * Get data news latest month random
     *
     * @return \App\Models\Post
     */
    public function latesNewstMonth(int $limit)
    {
        return $this->model
                    ->with(['user','categoryPost','getCommentName'])
                    ->where('type_post','=','news')
                    ->where('status', '=', Post::STATUS_PUBLISH)
                    ->orderBy('id','desc')
                    ->limit($limit)
                    ->get();
    }

    /**
     * Get data post high viwer article latest month
     *
     * @param int $limit
     * @return \App\Models\Post
     */
    public function highViewerArticleLatestMonth(int $limit)
    {
        return $this->model
                    ->with(['user','getCommentName'])
                    ->withCount('viewer')
                    ->where('type_post','=','article')
                    ->where('created_at','>=',date('Y-m-d H:i:s', strtotime("-3 month")))
                    ->where('status', '=', Post::STATUS_PUBLISH)
                    ->orderBy('viewer_count','desc')
                    ->limit($limit)
                    ->get();
    }

    /**
     * Get data post highland random
     *
     * @param int $limit
     * @return \App\Models\Post
     */
    public function latestArticle(int $limit)
    {
        return $this->model
                    ->with(['user','getCommentName'])
                    ->where('type_post','=','article')
                    ->where('status', '=', Post::STATUS_PUBLISH)
                    ->latest('id')
                    ->limit($limit)
                    ->get();
    }
}
