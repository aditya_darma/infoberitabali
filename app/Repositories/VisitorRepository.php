<?php

namespace App\Repositories;

use App\Models\Visitor;
use Illuminate\Support\Facades\DB;

class VisitorRepository {
    protected $model;

    public function __construct(
        Visitor $visitor
    )
    {
        $this->model = $visitor;
    }

    /**
     * Data count visitor group by date
     *
     * @param array $data
     * @return \App\Models\Visitor
     */
    public function getCountVisitorGroupByDate(array $data)
    {
        return $this->model
                    ->where('date','>=',$data['date_start'])
                    ->where('date','<=',$data['date_end'])
                    ->select('date', DB::raw('count(*) AS total'))
                    ->groupBy('date')
                    ->get();
    }
}