<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'size_type' => 'required',
            'date_start' => 'required',
            'date_end' => 'required',
            'image' => 'nullable|file|max:5000',
            'link' => 'required',
            'status' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nama iklan tidak boleh kosong',
            'size_type.required' => 'Ukuran iklan belum dipilih',
            'image.file' => 'Photo harus berupa file',
            'image.max' => 'Photo maximun 5MB',
            'date_start.required' => 'Tanggal mulai tidak boleh kosong',
            'date_end.required' => 'Tanggal berenti tidak boleh kosong',
            'link.required' => 'Link tidak boleh kosong',
            'status.required' => 'Status belum dipilih'
        ];
    }
}
