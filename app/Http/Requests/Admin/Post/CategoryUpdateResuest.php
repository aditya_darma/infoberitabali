<?php

namespace App\Http\Requests\Admin\Post;

use App\Traits\JsonValidateResponse;
use Illuminate\Foundation\Http\FormRequest;

class CategoryUpdateResuest extends FormRequest
{
    use JsonValidateResponse;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'active' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Nama tidak boleh kosong',
            'active.required' => 'Status tidak boleh kosong',
        ];
    }
}
