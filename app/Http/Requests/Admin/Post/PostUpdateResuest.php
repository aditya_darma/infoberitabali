<?php

namespace App\Http\Requests\Admin\Post;

use App\Traits\JsonValidateResponse;
use Illuminate\Foundation\Http\FormRequest;

class PostUpdateResuest extends FormRequest
{
    use JsonValidateResponse;
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'category' => 'required',
            'title' => 'required',
            'image' => 'nullable|file|max:5000',
            'keyword' => 'required',
            'status' => 'required|in:pending,publish'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'type.required' => 'Tipe tidak boleh kosong',
            'category.required' => 'Kategori tidak boleh kosong',
            'title.required' => 'Judul belum dipilih',
            'image.file' => 'Photo harus berupa file',
            'image.max' => 'Photo maximun 5MB',
            'keyword.required' => 'Keyword tidak boleh kosong',
            'status.required' => 'Status belum dipilih',
            'status.in' => 'Status tidak tersedia',
        ];
    }
}
