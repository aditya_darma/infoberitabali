<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Visitor as VisitorModel;
use Illuminate\Http\Request;

class CountVisitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        VisitorModel::firstOrCreate([
            'ip' => $request->ip(),
            'user_agent' => $request->server('HTTP_USER_AGENT'),
            'date' => date('Y-m-d')
        ]);
        
        return $next($request);
    }
}
