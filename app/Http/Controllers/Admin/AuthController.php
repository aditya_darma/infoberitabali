<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\LoginPostRequest;
use App\Services\AuthService;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(
        AuthService $authService
    )
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->authService = $authService;
    }
    
    /**
     * Halaman login admin
     * 
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('admin.auth.login');
    }

    /**
     * Authentikasi login admin
     * 
     * @param \App\Http\Requests\Admin\Auth\LoginPostRequest $request
     * @return mixed
     */
    public function loginPost(LoginPostRequest $request)
    {
        $request->validated();
        
        $result = $this->authService->loginAdmin([
            'email' => $request->email,
            'password' => $request->password,
            'remember' => $request->remember ? true : false
        ]);

        if ($result) {
            return redirect()->route('admin.dashboard');
        }
        
        return redirect()->back()->with('error', 'Email / Password salah!'); 
    }

    /**
     * Logout admin
     * 
     * @return mixed
     */
    public function logout()
    {
        $result = $this->authService->logoutAdmin();

        if ($result) {
            return redirect()->route('admin.login');
        }

        return redirect()->back();
    }
}
