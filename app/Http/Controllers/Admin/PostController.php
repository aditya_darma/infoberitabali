<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Post\CommentStoreRequest;
use App\Http\Requests\Admin\Post\PostStoreResuest;
use App\Http\Requests\Admin\Post\PostUpdateResuest;
use App\Services\Post\CommentService;
use App\Services\Post\PostService;
use App\Traits\Response;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PostController extends Controller
{
    use Response;

    protected $postService;
    protected $commentService;

    public function __construct(
        PostService $postService,
        CommentService $commentService
    )
    {
        $this->postService = $postService;
        $this->commentService = $commentService;
    }
    
    /**
     * Display data post.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.post.index');
    }

    /**
     * list data post
     *
     * @return \Yajra\DataTables\Facades\DataTables
     */
    public function datatable()
    {
        $result = $this->postService->listAllLatest();
        
        return DataTables::of($result->getResult())
                        ->addIndexColumn()
                        ->editColumn('title',function($data){
                            return $data->title;
                        })
                        ->editColumn('type',function($data){
                            return typePost()[$data->type_post];
                        })
                        ->addColumn('category',function($data){
                            return $data->categoryPost->name;
                        })
                        ->editColumn('author',function($data){
                            return $data->user->name;
                        })
                        ->editColumn('created_at',function($data){
                            return $data->created_at->format('d-m-Y H:i:s');
                        })
                        ->editColumn('highland',function($data){
                            return ($data->highland) ? 'Highlight' : 'Biasa';
                        })
                        ->editColumn('status',function($data){
                            return $data->status;
                        })
                        ->addColumn('status_text',function($data){
                            return statusPost()[$data->status];
                        })
                        ->addColumn('action',function($data){
                            return '<div class="dropdown d-inline">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-cog"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item has-icon" href="'. route($data->type_post . '.slug',['slug' => $data->slug]) .'" target="_blank"><i class="fas fa-eye"></i> View</a>
                                            <a class="dropdown-item has-icon" href="'.route('admin.post.comment.index',['id' => $data->id]).'"><i class="fas fa-comments"></i> Komentar</a>
                                            <a class="dropdown-item has-icon" href="'.route('admin.post.edit',['id' => $data->id]).'"><i class="fas fa-pencil-alt"></i> Edit</a>
                                            <a class="dropdown-item has-icon delete" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-trash-alt"></i> Delete</a>
                                        </div>
                                    </div>';
                        })
                        ->rawColumns(['action'])
                        ->make(true);
    }

    /**
     * Create data post.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->postService->create()->getResult();
        $data['title'] = 'Tambah Artikel';
        $data['url'] = route('admin.post.store');
        
        return view('admin.post.form', $data);
    }

    /**
     * Store data post.
     *
     * @param  \App\Http\Requests\Admin\Post\PostStoreResuest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PostStoreResuest $request)
    {
        $result = $this->postService->store([
            'type' => $request->type,
            'category' => $request->category,
            'title' => $request->title,
            'content' => $request->content,
            'image' => $request->file('image'),
            'extension' => $request->file('image')->getClientOriginalExtension(),
            'keyword' => $request->keyword,
            'highland' => $request->highland ? 1 : 0,
            'comment_reply' => true,
            'status' => $request->status
        ]);
        
        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    /**
     * Show for editing data post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->postService->edit($id)->getResult();
        $data['title'] = 'Edit Artikel';
        $data['url'] = route('admin.post.update',['id' => $id]);
        return view('admin.post.form', $data);
    }

    /**
     * Update data post.
     *
     * @param  \App\Http\Requests\Admin\Post\PostUpdateResuest  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PostUpdateResuest $request, $id)
    {
        $result = $this->postService->update($id, [
            'type' => $request->type,
            'category' => $request->category,
            'title' => $request->title,
            'content' => $request->content,
            'image' => $request->hasFile('image') ? $request->file('image') : null,
            'keyword' => $request->keyword,
            'highland' => $request->highland ? 1 : 0,
            'comment_reply' => true,
            'status' => $request->status
        ]);

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    /**
     * Delete data post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $result = $this->postService->delete($id);

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

        
    /**
     * Display data comment post.
     *
     * @param integer $id
     * @return \Illuminate\Http\Response
     */
    public function comment(int $id)
    {
        $data = $this->postService->getPost($id)->getResult();
        
        return view('admin.post.comment', $data);
    }

    /**
     * list data comment post
     *
     * @param integer $id
     * @return \Yajra\DataTables\Facades\DataTables
     */
    public function commentDatatable(int $id)
    {
        $result = $this->commentService->listCommentLatestByIdPost($id)->getResult();
        
        return DataTables::of($result)
                                    ->addIndexColumn()
                                    ->editColumn('title',function($data){
                                        return $data->post->title;
                                    })
                                    ->editColumn('comment',function($data){
                                        return $data->comment;
                                    })
                                    ->editColumn('created_at',function($data){
                                        return $data->created_at->format('d-m-Y H:i:s');
                                    })
                                    ->editColumn('user',function($data){
                                        return $data->user->name;
                                    })
                                    ->editColumn('active',function($data){
                                        return $data->active;
                                    })
                                    ->addColumn('action',function($data){
                                        $action = '<div class="dropdown d-inline">
                                                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-cog"></i>
                                                    </button>
                                                    <div class="dropdown-menu">';
                                                        if ($data->active == 1) {
                                                            $action .= '<a class="dropdown-item has-icon hide-comment" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-eye"></i> Sembunyikan</a>';
                                                        }else{
                                                            $action .= '<a class="dropdown-item has-icon show-comment" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-eye"></i> Tampilkan</a>';
                                                        }
                                                        if ($data->user_id == auth()->user()->id) {
                                                            $action .= '<a class="dropdown-item has-icon edit-comment" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-pencil-alt"></i> Edit</a>';
                                                        }
                                                        $action .= '<a class="dropdown-item has-icon delete-comment" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-trash-alt"></i> Delete</a>
                                                    </div>
                                                </div>';
                                        return $action;
                                    })
                                    ->rawColumns(['action'])
                                    ->make(true);
    }

    /**
     * Store comment from admin
     *
     * @param CommentStoreRequest $request
     * @param integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentStore(CommentStoreRequest $request, int $id)
    {
        $request->merge(['id' => $id]);
        $result = $this->commentService->storePostComment($request->all());

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    /**
     * Edit comment for admin
     *
     * @param integer $id
     * @param integer $comment
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentEdit(int $id, int $comment)
    {
        $result = $this->commentService->getCommentById($comment);

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    /**
     * Update comment for admin
     *
     * @param CommentStoreRequest $request
     * @param integer $id
     * @param integer $comment
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentUpdate(CommentStoreRequest $request, int $id, int $comment)
    {
        $result = $this->commentService->updatePostComment($request->all(), $comment);

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
}
