<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Post\CommentService;
use App\Traits\Response;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CommentController extends Controller
{
    use Response;

    protected $commentService;

    public function __construct(
        CommentService $commentService
    )
    {
        $this->commentService = $commentService;
    }
    
    /**
     * Display data post.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.comment.index');
    }

    /**
     * list data post
     *
     * @return \Yajra\DataTables\Facades\DataTables
     */
    public function datatable()
    {
        $result = $this->commentService->listAllCommentLatest()->getResult();
        
        return DataTables::of($result)
                        ->addIndexColumn()
                        ->editColumn('title',function($data){
                            return $data->post->title;
                        })
                        ->editColumn('comment',function($data){
                            return $data->comment;
                        })
                        ->editColumn('created_at',function($data){
                            return $data->created_at->format('d-m-Y H:i:s');
                        })
                        ->editColumn('user',function($data){
                            return $data->user->name;
                        })
                        ->editColumn('active',function($data){
                            return $data->active;
                        })
                        ->addColumn('action',function($data){
                            $action = '<div class="dropdown d-inline">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-cog"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item has-icon" href="'. route($data->post->type_post . '.slug',['slug' => $data->post->slug]) .'"><i class="fas fa-eye"></i> Post Comment</a>';
                                            if ($data->active == 1) {
                                                $action .= '<a class="dropdown-item has-icon hide-comment" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-pencil-alt"></i> Sembunyikan</a>';
                                            }else{
                                                $action .= '<a class="dropdown-item has-icon show-comment" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-pencil-alt"></i> Tampilkan</a>';
                                            }
                                            $action .= '<a class="dropdown-item has-icon delete-comment" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-trash-alt"></i> Delete</a>
                                        </div>
                                    </div>';
                            return $action;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
    }

    /**
     * Show comment post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $result = $this->commentService->showComment($id);

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    /**
     * Hide comment post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function hide($id)
    {
        $result = $this->commentService->hideComment($id);

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    /**
     * Delete comment post.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $result = $this->commentService->deleteComment($id);

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
}
