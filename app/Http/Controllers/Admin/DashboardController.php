<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\VisitorService;
use App\Traits\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    use Response;
    
    protected $visitorService;

    public function __construct(
        VisitorService $visitorService
    )
    {
        $this->visitorService = $visitorService;
    }
    
    /**
     * Dashbord admin
     *
     * @return void
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    /**
     * Dashbord admin
     *
     * @return void
     */
    public function visitor()
    {
        $result = $this->visitorService->countVisitorByDate([
            'date_start' => Carbon::now()->subMonth()->format('Y-m-d'),
            'date_end' => Carbon::now()->format('Y-m-d')
        ]);
        
        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
}
