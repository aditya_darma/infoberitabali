<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AdsStoreRequest;
use App\Http\Requests\Admin\AdsUpdateRequest;
use App\Services\AdsService;
use App\Traits\Response;
use App\Utilities\FormatDateTime;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AdsController extends Controller
{
    use Response;
    use FormatDateTime;
    
    protected $adsService;

    public function __construct(
        AdsService $adsService
    )
    {
        $this->adsService = $adsService;
    }
    
    public function index()
    {
        return view('admin.ads.index');
    }
    
    public function datatable()
    {
        $result = $this->adsService->getAll();

        return DataTables::of($result->getResult())
                            ->addIndexColumn()
                            ->editColumn('name',function($data){
                                return $data->name;
                            })
                            ->editColumn('link',function($data){
                                return '<a href="'. $data->link .'" target="_blank">'. $data->link .'</a>';
                            })
                            ->editColumn('date_start',function($data){
                                return $data->date_start->format('d-m-Y');
                            })
                            ->editColumn('type_ads',function($data){
                                return typeAds()[$data->size_type];
                            })
                            ->editColumn('date_end',function($data){
                                return $data->date_end->format('d-m-Y');
                            })
                            ->addColumn('action',function($data){
                                return '<div class="dropdown d-inline">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-cog"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item has-icon" href="'. route('admin.ads.edit',['id' => $data->id]) .'"><i class="fas fa-pencil-alt"></i> Edit</a>
                                                <a class="dropdown-item has-icon delete" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-trash-alt"></i> Delete</a>
                                            </div>
                                        </div>';
                            })
                            ->rawColumns(['link','action'])
                            ->make(true);
    }
    
    public function create()
    {
        $data = $this->adsService->create()->getResult();
        $data['title'] = 'Tambah Iklan';
        $data['url'] = route('admin.ads.store');

        return view('admin.ads.form', $data);
    }

    public function store(AdsStoreRequest $request)
    {
        $request->merge([
            'date_start' => $this->formatDashYMD($request->date_start),
            'date_end' => $this->formatDashYMD($request->date_end),
            'image' => $request->hasFile('image') ? $request->file('image') : null,
        ]);

        $result = $this->adsService->store($request->all());

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    public function edit(int $id)
    {
        $data = $this->adsService->edit($id)->getResult();
        $data['title'] = 'Edit Iklan';
        $data['url'] = route('admin.ads.update',['id' => $id]);
        
        return view('admin.ads.form', $data);
    }

    public function update(AdsUpdateRequest $request, $id)
    {
        $request->merge([
            'date_start' => $this->formatDashYMD($request->date_start),
            'date_end' => $this->formatDashYMD($request->date_end),
            'image' => $request->hasFile('image') ? $request->file('image') : null,
        ]);

        $result = $this->adsService->update($id, $request->all());

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    public function delete(int $id)
    {
        $result = $this->adsService->delete($id);
        
        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
}
