<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Post\CategoryStoreResuest;
use App\Http\Requests\Admin\Post\CategoryUpdateResuest;
use App\Services\Post\CategoryService;
use App\Traits\Response;
use Yajra\DataTables\Facades\DataTables;

class CategoryPostController extends Controller
{
    use Response;
    
    protected $categoryService;
    
    public function __construct(
        CategoryService $categoryService
    )
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Page list data category post
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.master.category-post.index');
    }

    /**
     * list data category post
     *
     * @return \Yajra\DataTables\Facades\DataTables
     */
    public function datatable()
    {
        $result = $this->categoryService->listAll();

        return DataTables::of($result->getResult())
                        ->addIndexColumn()
                        ->editColumn('name',function($data){
                            return $data->name;
                        })
                        ->editColumn('active',function($data){
                            return ($data->active) ? "Aktif" : 'Tidak Aktif';
                        })
                        ->addColumn('action',function($data){
                            return '<div class="dropdown d-inline">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-cog"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item has-icon edit" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-pencil-alt"></i> Edit</a>
                                            <a class="dropdown-item has-icon delete" href="javascript:void(0)" data-id="'.$data->id.'"><i class="fas fa-trash-alt"></i> Delete</a>
                                        </div>
                                    </div>';
                        })
                        ->rawColumns(['action'])
                        ->make(true);
    }

    /**
     * Create data category post
     *
     * @param \App\Http\Requests\Admin\Post\CategoryStoreResuest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryStoreResuest $request)
    {
        $result = $this->categoryService->store([
            'name' => $request->name,
            'active' => $request->active
        ]);
        
        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
    
    /**
     * Get data category post
     *
     * @param integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(int $id)
    {
        $result = $this->categoryService->findId($id);
        
        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    /**
     * Update data category post
     *
     * @param integer $id
     * @param \App\Http\Requests\Admin\Post\CategoryUpdateResuest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CategoryUpdateResuest $request, int $id)
    {
        $result = $this->categoryService->update($id, [
            'name' => $request->name,
            'active' => $request->active
        ]);
        
        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
        
    /**
     * Delete data category post
     *
     * @param integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        $result = $this->categoryService->delete($id);
        
        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
}
