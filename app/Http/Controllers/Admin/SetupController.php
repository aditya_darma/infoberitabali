<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\SetupService;
use App\Traits\Response;
use Illuminate\Http\Request;

class SetupController extends Controller
{
    use Response;
    
    protected $setupService;

    public function __construct(
        SetupService $setupService
    )
    {
        $this->setupService = $setupService;
    }
    
    public function aboutUs()
    {
        $data = $this->setupService->aboutUs()->getResult();
        
        return view('admin.setup.about', $data);
    }
    
    public function aboutUsUpdate(Request $request)
    {
        $result = $this->setupService->aboutUsUpdate($request->all());

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
    
    public function footer()
    {
        $data = $this->setupService->footer()->getResult();
        
        return view('admin.setup.footer', $data);
    }
    
    public function footerUpdate(Request $request)
    {
        $result = $this->setupService->footerUpdate($request->all());

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
}
