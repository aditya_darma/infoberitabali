<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginVisitorRequest;
use App\Http\Requests\Auth\RegisterVisitorRequest;
use App\Services\AuthService;
use App\Services\Post\PostService;
use App\Traits\Response;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use Response;
    
    protected $authService;

    public function __construct(
        AuthService $authService
    )
    {
        $this->authService = $authService;
    }

    public function register(RegisterVisitorRequest $request)
    {
        $request->validated();

        $result = $this->authService->registerVisitor($request->all());

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    public function login(LoginVisitorRequest $request)
    {
        $request->validated();

        $result = $this->authService->loginVisitor($request->all());

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    public function logout()
    {
        $result = $this->authService->logoutVisitor();

        return redirect()->route('index');
    }

    public function verification(Request $request)
    {
        if (! $request->token) {
            return redirect()->route('index')->with('error', 'Token tidak ditemukan');
        }
        
        $result = $this->authService->verification($request->all());
        if ($result->isSuccess()) {
            return redirect()->route('index')->with('success', $result->getMessage());
        }

        return redirect()->route('index')->with('error', 'Akun gagal di aktivasi');
    }
}