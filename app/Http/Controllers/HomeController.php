<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactStoreRequest;
use App\Services\NotificationService;
use App\Services\Post\PostService;
use App\Services\SetupService;
use App\Traits\Response;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use Response;
    
    protected $postService;
    protected $setupService;
    protected $notificationService;
    
    public function __construct(
        PostService $postService,
        SetupService $setupService,
        NotificationService $notificationService
    )
    {
        $this->postService = $postService;
        $this->setupService = $setupService;
        $this->notificationService = $notificationService;
    }

    public function index()
    {
        $data = $this->postService->homeWebsite()->getResult();
        
        return view('index', $data);
    }
    
    public function about()
    {
        $data = $this->setupService->aboutUs()->getResult();
        
        return view('about', $data);
    }

    public function contact()
    {
        return view('contact');
    }

    public function contactStore(ContactStoreRequest $request)
    {
        $result = $this->notificationService->sendEmailContact($request->all());

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
}
