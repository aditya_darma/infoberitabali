<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\PostCommentRequest;
use App\Models\Post;
use App\Services\Post\CommentService;
use App\Services\Post\PostService;
use App\Traits\Response;
use Illuminate\Http\Request;

class PostController extends Controller
{
    use Response;

    protected $postService;
    protected $commentService;

    public function __construct(
        PostService $postService,
        CommentService $commentService
    )
    {
        $this->postService = $postService;
        $this->commentService = $commentService;
    }

    /**
     * Detail post
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function slug($slug)
    {
        $data = $this->postService->postDetailBySlug($slug)->getResult();
        
        return view('post.slug', $data);
    }

    /**
     * Pencarian post
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if ($request->ajax()) {
            $result = $this->postService->getPostSearchView($request->search);
        
            return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
        }
        $data = $this->postService->getPostSearch($request->search)->getResult();
        
        return view('post.index', $data);
    }

    /**
     * Post type news
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\JsonResponse
     */
    public function news(Request $request)
    {
        if ($request->ajax()) {
            $result = $this->postService->getPostNewsView($request->search);
        
            return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
        }
        $data = $this->postService->getPostNews($request->search)->getResult();
        
        return view('post.index', $data);
    }

    /**
     * Post type article
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\JsonResponse
     */
    public function article(Request $request)
    {
        if ($request->ajax()) {
            $result = $this->postService->getPostArticleView($request->search);
        
            return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
        }
        $data = $this->postService->getPostArticle($request->search)->getResult();
        
        return view('post.index', $data);
    }

    /**
     * Post type article popular
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\JsonResponse
     */
    public function articlePopular(Request $request)
    {
        if ($request->ajax()) {
            $result = $this->postService->getPostArticlePopularView($request->search);
        
            return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
        }
        $data = $this->postService->getPostArticlePopular($request->search)->getResult();
        
        return view('post.index', $data);
    }

    /**
     * Post type highland
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\JsonResponse
     */
    public function highland(Request $request)
    {
        if ($request->ajax()) {
            $result = $this->postService->getPostHighlandView($request->search);
        
            return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
        }
        $data = $this->postService->getPostHighland($request->search)->getResult();
        
        return view('post.index', $data);
    }

    /**
     * List post by category
     *
     * @param Request $request
     * @param string $slug
     * @return \Illuminate\Http\Response
     * @return \Illuminate\Http\JsonResponse
     */
    public function category(Request $request, $slug)
    {
        if ($request->ajax()) {
            $result = $this->postService->getPostCategoryView($slug);
        
            return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
        }
        $data = $this->postService->getPostCategory($slug)->getResult();
        
        return view('post.index', $data);
    }

    /**
     * Store comment post
     *
     * @param PostCommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function comment(PostCommentRequest $request)
    {
        $request->validated();
        $result = $this->commentService->storePostComment($request->all());

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }

    /**
     * Get comment post
     *
     * @param integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentPost(int $id)
    {
        $result = $this->commentService->getPostComment($id);

        return $this->responseJson($result->isSuccess(), $result->getMessage(), $result->getResult(), $result->getCode());
    }
}
