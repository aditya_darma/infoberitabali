<?php

use App\Models\Ads;
use App\Models\CategoryPost;
use App\Models\Setup;
use App\Repositories\AdsRepository;
use Illuminate\Support\Facades\Cache;

function typePost()
{
    return [
        'news' => 'News',
        'article' => 'Article'
    ];
}

function statusPost()
{
    return [
        'pending' => 'Pending',
        'publish' => 'Publish'
    ];
}

function statusPostBadge()
{
    return [
        'pending' => '<div class="badge badge-warning">Pending</div>',
        'publish' => '<div class="badge badge-primary">Publish</div>'
    ];
}

function listCategoryActive()
{
    return CategoryPost::where('active','=',1)->pluck('name','slug');
}

function setup($name)
{
    return Cache::remember($name, 3600, function () use ($name){
        return Setup::where('name','=',$name)->first()->value ?? null;
    });
}

function randomPostStyle()
{
    $array = [
        [4,4,4,5,7,4,4,4,7,5],
        [4,4,4,7,5,5,7,4,4,4],
        [5,7,4,4,4,7,5,4,4,4],
        [5,7,4,4,4,4,4,4,5,7],
        [4,4,4,5,7,7,5,4,4,4]
    ];

    return $array[array_rand($array)];
}

function typeAds()
{
    return [
        1 => '820 x 100',
        2 => '1709 x 1459'
    ];
}


function sizeAds()
{
    return [
        1 => [820,100],
        2 => [1709,1459]
    ];
}

function pathImagePost(string $image)
{
    return asset('content/photo/post/'.$image);
}

function adsSizeFirst($ads)
{
    return '<div class="container ads-text1">
                <a href="'. $ads->link .'" target="_blank">
                    <img class="img-fluid img-custom" src="'. asset('content/photo/ads/'. $ads->image) .'" style="width: 100%; margin-top: 5%;" alt="">
                </a>
                <p class="ads-text1">Ads</p>
            </div>';
}

function adsSizeSecond($ads)
{
    return '<div class="container ads-text2">
                <a href="'. $ads->link .'" target="_blank">
                    <img class="img-fluid img-custom2" src="'. asset('content/photo/ads/'. $ads->image) .'" style="width: 100%; margin-top: 5%;"
                        alt="">
                </a>
                <p class="ads-text2">Ads</p>
            </div>';
}

function adsSizeSecondPost($ads)
{
    return '<div class="container ads-text2">
            <div class="row d-flex justify-content-center">
                <div class="col-md-8">
                    <a href="'. $ads->link .'" target="_blank">
                        <img class="img-fluid img-custom2" src="' . asset('content/photo/ads/'. $ads->image) . '" style="width: 100%; margin-top: 5%;" alt="">
                    </a>
                    <p class="ads-text2">Ads</p>
                </div>
            </div>
        </div>';
}

function replaceForViewAds(string $content)
{
    $count_ads_content = substr_count($content, 'iklan_');
    $no = 0;

    $ads = new AdsRepository(new Ads());
    $data = $ads->getAllSecondActiveTodayTake(date('Y-m-d'), $count_ads_content);
    foreach ($data as $key => $value) {
        $no = $key;
        $replace = '{iklan_'. (intval($key) + 1).'}';
        $content = str_replace($replace, adsSizeSecondPost($value), $content);
    }
    
    for ($i=$no; $i < $count_ads_content; $i++) {
        $replace = '{iklan_'. ($i + 1) .'}';
        $content = str_replace($replace, '', $content);
    }

    return $content;
}

function randomAdsFirst()
{
    $ads = new AdsRepository(new Ads());
    $data = $ads->getAllFirstActiveTodayFirst(date('Y-m-d'));

    return $data ? adsSizeFirst($data) : null;
}

function randomAdsSecond()
{
    $ads = new AdsRepository(new Ads());
    $data = $ads->getAllSecondActiveTodayFirst(date('Y-m-d'));

    return $data ? adsSizeSecond($data) : null;
}

function isLogin()
{
    return auth()->check();
}

function isAdmin()
{
    return auth()->user()->role == 'admin';
}

function isVisitor()
{
    return auth()->user()->role == 'visitor';
}