<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostViewer extends Model
{
    protected $table = 'post_viewer';

    protected $fillable = [
        'post_id',
        'ip',
        'user_agent',
        'date'
    ];
}
