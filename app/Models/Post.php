<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Post extends Model
{
    use HasSlug;

    const STATUS_PUBLISH = 'publish';
    const STATUS_PENDING = 'pending';

    protected $table = 'post';

    protected $fillable = [
        'type_post',
        'category_post_id',
        'title',
        'content',
        'image',
        'keyword',
        'highland',
        'comment_reply',
        'status',
        'user_id'
    ];

    protected $dates = [
        'created_at'
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Relation to category post
     */
    public function categoryPost()
    {
        return $this->belongsTo(CategoryPost::class, 'category_post_id', 'id');
    }

    /**
     * Relation to user
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Relation to comment
     */
    public function viewer()
    {
        return $this->hasMany(PostViewer::class, 'post_id', 'id');
    }

    /**
     * Relation to comment
     */
    public function comment()
    {
        return $this->hasMany(PostComment::class, 'post_id', 'id');
    }

    public function countView()
    {
        if (isset($this->viewer_count)) {
            $viewer = $this->viewer_count;
        } else {
            $viewer = $this->viewer()->count();
        }


        if ($viewer > 1000) {
            return ($viewer / 1000).'K';
        }

        return $viewer;
    }

    public function countComment()
    {
        if (isset($this->comment_count)) {
            $comment = $this->comment_count;
        } else {
            $comment = $this->comment()->count();
        }


        if ($comment > 1000) {
            return ($comment / 1000).'K';
        }

        return $comment;
    }

    public function countCommentActive()
    {
        if (isset($this->comment_count)) {
            $comment = $this->comment_count;
        } else {
            $comment = $this->comment->where('active','=',1)->count();
        }


        if ($comment > 1000) {
            return ($comment / 1000).'K';
        }

        return $comment;
    }

    public function getCommentName()
    {
        return $this->hasMany(PostComment::class, 'post_id', 'id')->with(['user'])->where('active','=',1)->latest()->limit(3);
    }
}
