<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $table = 'ads';

    protected $fillable = [
        'name',
        'size_type',
        'date_start',
        'date_end',
        'image',
        'link',
        'active'
    ];

    protected $dates = [
        'date_start',
        'date_end'
    ];
}
