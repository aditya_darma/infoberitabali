<?php

namespace App\Traits;

use App\Traits\ResultService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Exception;
use Illuminate\Support\Facades\Request;

trait ExceptionCustom
{
	use ResultService;

    /**
     * Exception Response
     *
     * @param Exception $exception
     * @return App\Traits\ResultService
     */
	public function exceptionResponse(Exception $exception)
	{
		if ($exception instanceof QueryException) {
            if ($exception->errorInfo[1] == 1451) {
                return $this->setSuccess(false)
                            ->setMessage('Data masih terpakai di Data Lain!')
                            ->setCode(400);
            }
        }
        if ($exception instanceof ModelNotFoundException) {
            if (!request()->expectsJson()) {
                return abort(404);
            }
            return $this->setSuccess(false)
                        ->setMessage('Data tidak ditemukan!')
                        ->setCode(404);
        }
        if (env('APP_DEBUG')) {
            return $this->setSuccess(false)
                        ->setMessage($exception->getMessage())
                        ->setCode(400);
        }
        
        return $this->setSuccess(false)
                        ->setMessage('Terjadi suatu kesalahan!')
                        ->setCode(400);
	}
}
