<?php

namespace App\Traits;

trait Response{

    public function responseJson($status = true, $msg = '', $data = [], $code = null)
    {
        if(is_null($code)){
            $http_code = $status ? 200 : 400;
        }else{
            $http_code = $code;
        }
        
        return response()->json([
            'status' => $status,
            'code' => $http_code,
            'message' => $msg,
            'data' => $data,
        ], $http_code);
    }

}
