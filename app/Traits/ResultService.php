<?php

namespace App\Traits;

trait ResultService
{
	private $result = null;
	private $isSuccess = false;
	private $message = null;
	private $code = null;

	public function setResult($result)
	{
		$this->result = $result;

		return $this;
	}

	public function getResult()
	{
		return $this->result;
	}

	public function setSuccess($isSuccess)
	{
		$this->isSuccess = $isSuccess;

		return $this;
	}

	public function isSuccess()
	{
		return $this->isSuccess;
	}

	public function setMessage($message)
	{
		$this->message = $message;

		return $this;
	}

	public function getMessage()
	{
		return $this->message;
	}

	public function setCode($code)
	{
		$this->code = $code;

		return $this;
	}

	public function getCode()
	{
		return $this->code;
	}
}
