<?php

namespace App\Utilities;

trait FormatDateTime
{
    public function formatDashYMD($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    public function formatDashDMY($date)
    {
        return date('Y-m-d', strtotime($date));
    }
}