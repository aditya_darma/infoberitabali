/****
 * ******************************************
 * @views: admin/master/category-post
 * *******************************************
 ****/

function table(url)
{
    $('#table').DataTable({
        serverSide: true,
        ordering: true,
        autoWidth: false,
        responsive: true,
        processing: true,
        oLanguage: {sProcessing: loading},
        ajax: {
            url: url
        },
        columns: [
            { data: 'DT_RowIndex',name: 'DT_RowIndex', width : '10px' },  
            { data: 'name',name: 'nama' },
            { data: 'active',name: 'active', width: '10%' },
            { data: 'action', name: 'action', ordering: false, width: '50px' },
        ],
        columnDefs: [            
            { targets: [0], className: "text-center" },
            { targets: [3], className: "text-center" },
        ]
    });
}

function submit(url, data)
{
    ajaxPost(url, data, 'json')
    .done(function(res){
        if (res.status) {
            $('#modal-form').modal('hide');
            reloadTable('#table');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function edit(url)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            var data = res.data;
            $('#id').val(data.id);
            $('#name').val(data.name);
            $('#active').val(data.active);

            $('#modal-form').modal('show');
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function destroy(url)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            reloadTable('#table');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}