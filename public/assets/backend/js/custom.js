/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 * 
 */

"use strict";

let loading = '<i class="bx bx-hourglass bx-spin font-size-16 align-middle mr-2 loading"></i>';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function ajaxGet(url, data={}){
    return $.ajax({
        type: 'GET',
        url: url,
        data: data,
        dataType: 'json',
        beforeSend:function()
        {
            showBlockUI();
        },
        complete:function()
        {
            closeBlockUI();
        },
    })
    .fail(function(res){
        swal({
            title: 'Fail!',
            text: res.responseJSON.message,
            icon: 'error'
        });
    })
}

function ajaxPost(url, data, datatype = 'json'){
    return $.ajax({
        type: 'post',
        url: url,
        data: data,
        dataType: datatype,
        contentType:false,
        processData:false,
        beforeSend:function()
        {
            showBlockUI();
        },
        complete:function()
        {
            closeBlockUI();
        }
    })
    .fail(function(res){
        if (res.status == 422) {
            $.each(res.responseJSON.errors, function(i,v){
                $('.error-'+i).html(v);
                $('input[name='+i+']').keypress(function(){
                    $('.error-'+i).text('');
                }).change(function(){
                    $('.error-'+i).text('');
                });
                $('select[name='+i+']').change(function(){
                    $('.error-'+i).text('');
                });
                $('textarea[name='+i+']').keypress(function(){
                    $('.error-'+i).text('');
                });
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.responseJSON.message,
                icon: 'error'
            });
        } 
    })
}

function reloadTable(id) {
    var table = $(id).DataTable();
    table.cleanData;
    table.ajax.reload();
}

function time(time){
    var str = time.split(":");
    return str[0]+':'+str[1];
}

function convert_date(date){
    var date = date.split('-');
    return date[2]+ '-' +date[1]+ '-' +date[0];
}

function readURL(input, target) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
  
        reader.onload = function (e) {
            target.attr('src', e.target.result);
        }
  
        reader.readAsDataURL(input.files[0]);
    }
}

function showBlockUI() {
    $.blockUI({
        message: 'Mohon Tunggu ...',
        css: {
            'z-index': 10002,
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',
        }
    });
}

function closeBlockUI() {
    $.unblockUI();
}

$(".modal").on("hidden.bs.modal", function() {
    $(this).find('form').trigger("reset");
    $('.form-error').text('')
});

if(jQuery().summernote) {   
    $('.summernote').summernote({
        dialogsInBody: true,
        height: 500,
        toolbar: [
            ["style", ["style"]],
            ["font", ["bold", "underline", "clear"]],
            ["fontname", ["fontname"]],
            ["color", ["color"]],
            ["para", ["ul", "ol", "paragraph"]],
            ["table", ["table"]],
            ["insert", ["link", "picture", "video"]],
        ],
    });
}