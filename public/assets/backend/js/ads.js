/****
 * ******************************************
 * @views: admin/ads
 * *******************************************
 ****/

function table(url)
{
    $('#table').DataTable({
        serverSide: true,
        ordering: true,
        autoWidth: false,
        responsive: true,
        processing: true,
        oLanguage: {sProcessing: loading},
        ajax: {
            url: url
        },
        columns: [
            { data: 'DT_RowIndex',name: 'DT_RowIndex', width : '10px' },  
            { data: 'name',name: 'nama' },
            { data: 'link',name: 'link' },
            { data: 'type_ads',name: 'type_ads' },
            { data: 'date_start',name: 'date_start', width: '20%' },
            { data: 'date_end',name: 'date_end', width: '20%' },
            { data: 'action', name: 'action', ordering: false, width: '50px' },
        ],
        columnDefs: [            
            { targets: [0], className: "text-center" },
            { targets: [5], className: "text-center" },
        ]
    });
}

function submit(url, data, redirect)
{
    ajaxPost(url, data, 'json')
    .done(function(res){
        if (res.status) {
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            }).then(function(){
                location.href = redirect;
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function destroy(url)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            reloadTable('#table');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}