/****
 * ******************************************
 * @views: admin/post
 * *******************************************
 ****/

function table(url)
{
    $('#table').DataTable({
        serverSide: true,
        ordering: true,
        autoWidth: false,
        responsive: true,
        processing: true,
        oLanguage: {sProcessing: loading},
        ajax: {
            url: url
        },
        columns: [
            { data: 'DT_RowIndex',name: 'DT_RowIndex', ordering: false, width : '10px' },  
            { data: 'title',name: 'title' },
            { data: 'type',name: 'type' },
            { data: 'category',name: 'category' },
            { data: 'author',name: 'author' },
            { data: 'created_at',name: 'created_at' },
            { data: 'status',name: 'status', width: '10%' },
            { data: 'action', name: 'action', ordering: false, width: '50px' },
        ],
        columnDefs: [            
            { targets: [0], className: "text-center" },
            { targets: [7], className: "text-center" },
        ],
        createdRow: function ( row, data, index ) {
            if ( data.status == 'pending' ) {
                $('td', row).eq(6).addClass('bg-warning text-white');
                $('td', row).eq(6).text(data.status_text + ' | ' + data.highland);
            }else if(data.status == 'publish') {
                $('td', row).eq(6).addClass('bg-primary text-white');
                $('td', row).eq(6).text(data.status_text + ' | ' + data.highland);
            }
        },
    });
}

function submit(url, data, redirect)
{
    ajaxPost(url, data, 'json')
    .done(function(res){
        if (res.status) {
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            })
            .then(function(){
                location.href = redirect;
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function destroy(url)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            reloadTable('#table');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function tableComment(url)
{
    $('#table').DataTable({
        serverSide: true,
        ordering: true,
        autoWidth: false,
        responsive: true,
        processing: true,
        oLanguage: {sProcessing: loading},
        ajax: {
            url: url
        },
        columns: [
            { data: 'DT_RowIndex',name: 'DT_RowIndex', ordering: false, width : '10px' },  
            { data: 'comment',name: 'comment' },
            { data: 'created_at',name: 'created_at' },
            { data: 'user',name: 'user' },
            { data: 'active',name: 'active' },
            { data: 'action', name: 'action', ordering: false, width: '50px' },
        ],
        columnDefs: [            
            { targets: [0], className: "text-center" },
            { targets: [5], className: "text-center" },
        ],
        createdRow: function ( row, data, index ) {
            if ( data.active == 1) {
                $('td', row).eq(4).addClass('bg-primary text-white');
                $('td', row).eq(4).text('Terlihat' );
            }else {
                $('td', row).eq(4).addClass('bg-warning text-white');
                $('td', row).eq(4).text('Disembunyikan' );
            }
        },
    });
}


function show(url)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            reloadTable('#table');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function hide(url)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            reloadTable('#table');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function submitComment(url, data)
{
    ajaxPost(url, data, 'json')
    .done(function(res){
        if (res.status) {
            $('#modal-comment').modal('hide');
            $('#form-comment')[0].reset();
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            })
            .then(function(){
                reloadTable('#table');
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function editComment(url, action)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            var data = res.data;
            $('#comment').val(data.comment);

            $('#form-comment').attr('action', action);
            $('#modal-comment').modal('show');
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}