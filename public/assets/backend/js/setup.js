/****
 * ******************************************
 * @views: admin/setup
 * *******************************************
 ****/
 
function submit(url, data)
{
    ajaxPost(url, data, 'json')
    .done(function(res){
        if (res.status) {
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            })
            .then(function(){
                location.reload();
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}