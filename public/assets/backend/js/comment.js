/****
 * ******************************************
 * @views: admin/comment
 * *******************************************
 ****/

function table(url)
{
    $('#table').DataTable({
        serverSide: true,
        ordering: true,
        autoWidth: false,
        responsive: true,
        processing: true,
        oLanguage: {sProcessing: loading},
        ajax: {
            url: url
        },
        columns: [
            { data: 'DT_RowIndex',name: 'DT_RowIndex', ordering: false, width : '10px' },  
            { data: 'comment',name: 'comment' },
            { data: 'title',name: 'title' },
            { data: 'created_at',name: 'created_at' },
            { data: 'user',name: 'user' },
            { data: 'active',name: 'active' },
            { data: 'action', name: 'action', ordering: false, width: '50px' },
        ],
        columnDefs: [            
            { targets: [0], className: "text-center" },
            { targets: [6], className: "text-center" },
        ],
        createdRow: function ( row, data, index ) {
            if ( data.active == 1) {
                $('td', row).eq(5).addClass('bg-primary text-white');
                $('td', row).eq(5).text('Terlihat' );
            }else {
                $('td', row).eq(5).addClass('bg-warning text-white');
                $('td', row).eq(5).text('Disembunyikan' );
            }
        },
    });
}


function show(url)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            reloadTable('#table');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function hide(url)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            reloadTable('#table');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}

function destroy(url)
{
    ajaxGet(url)
    .done(function(res){
        if (res.status) {
            reloadTable('#table');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
        }
    });
}