"use strict";


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function ajaxGet(url, data={}, btn, text){
    return $.ajax({
        type: 'GET',
        url: url,
        data: data,
        dataType: 'json',
        beforeSend:function()
        {
            $(btn).attr('disabled', true).html('<i class="fas fa-spinner fa-spin fs-20"></i>');
        },
        complete:function()
        {
            $(btn).attr('disabled', false).text(text);
        },
    })
    .fail(function(res){
        swal({
            title: 'Error!',
            text: res.responseJSON.message,
            icon: 'error'
        });
    })
}

function ajaxPost(url, data, form, btn, text){
    return $.ajax({
        type: 'post',
        url: url,
        data: data,
        dataType: 'json',
        contentType:false,
        processData:false,
        beforeSend:function()
        {
            $(btn).attr('disabled', true).html('<i class="fas fa-spinner fa-spin fs-20"></i>');
        },
        complete:function()
        {
            $(btn).attr('disabled', false).text(text);
        }
    })
    .fail(function(res){
        if (res.status == 422) {
            $.each(res.responseJSON.errors, function(i,v){
                $(form + ' .error-'+i).html(v);
                $('input[name='+i+']').keypress(function(){
                    $('.error-'+i).text('');
                }).change(function(){
                    $('.error-'+i).text('');
                });
                $('select[name='+i+']').change(function(){
                    $('.error-'+i).text('');
                });
                $('textarea[name='+i+']').keypress(function(){
                    $('.error-'+i).text('');
                });
            });
        } else {
            swal({
                title: 'Error!',
                text: res.responseJSON.message,
                icon: 'error'
            });
        } 
    })
}