$('#form-login').submit(function(e){
    e.preventDefault();
    const url = $(this).attr('action');
    const data = new FormData(this);
    
    ajaxPost(url, data, '#form-login', '#btn-login', 'Login')
    .done(function(res){
        if (res.status) {
            $('#loginModal').modal('hide');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            }).then(function(){
                location.reload();
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
            $('#form-login input[name=password]').val('');
        }
    })
});

$('#form-register').submit(function(e){
    e.preventDefault();
    const url = $(this).attr('action');
    const data = new FormData(this);
    
    ajaxPost(url, data, '#form-register', '#btn-register', 'Daftar')
    .done(function(res){
        if (res.status) {
            $('#loginModal').modal('hide');
            swal({
                title: 'Success',
                text: res.message,
                icon: 'success'
            }).then(function(){
                location.reload();
            });
        } else {
            swal({
                title: 'Fail!',
                text: res.message,
                icon: 'error'
            });
            $('#form-register input[name=password]').val('');
            $('#form-register input[name=password_confirmation]').val('');
        }
    })
});

function showRegisterForm(){
    $('.loginBox').fadeOut('fast',function(){
        $('.registerBox').fadeIn('fast');
        $('.login-footer').fadeOut('fast',function(){
            $('.register-footer').fadeIn('fast');
        });
        $('.modal-title').html('Register with');
    }); 
    $('.error').removeClass('alert alert-danger').html('');
}

function showLoginForm(){
    $('#loginModal .registerBox').fadeOut('fast',function(){
        $('.loginBox').fadeIn('fast');
        $('.register-footer').fadeOut('fast',function(){
            $('.login-footer').fadeIn('fast');    
        });
        
        $('.modal-title').html('Login with');
    });       
     $('.error').removeClass('alert alert-danger').html(''); 
}

function openLoginModal(){
    showLoginForm();
    setTimeout(function(){
        $('#loginModal').modal('show');    
    }, 230);
    
}

function openRegisterModal(){
    showRegisterForm();
    setTimeout(function(){
        $('#loginModal').modal('show');    
    }, 230);
    
}