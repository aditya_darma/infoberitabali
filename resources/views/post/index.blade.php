@extends('layouts.frontend.app')

@section('title', $title)

@section('content')
<!-- Title -->
<div class="container" style="padding-top: 5%;">
    <p>
        <div class="row d-flex ">
            <div class="col-md-6">
                <h2 style="font-family: 'Playfair Display', serif; font-weight: 700;">{{ $title }}</h2>
            </div>
            <div class="col-md-6">
                <form action="{{ request()->fullUrl() }}" method="GET">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control mr-3" name="search" value="{{ request()->search }}" placeholder="Input Here" aria-label="Recipient's username" aria-describedby="basic-addon2" style="border-radius: 100px;">
                        <div class="input-group-append">
                            <button class="btn" type="submit" style="border-radius: 100px; color: white; background-color: #9e0a10;">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </p>
</div>
<!-- Title akhir -->

<!-- Line -->
<div class="container pt-2">
    <hr>
</div>
<!-- Akhir Line -->


<div class="container pt-4">
    <div class="row" id="list">
        {{ view('component.list-post', ['post' => $post, 'column' => randomPostStyle()]) }}
    </div>
</div>


<div class="col-md-12 text-center">
    <button class="btn btn-custom my-2 my-sm-0 ml-sm-4" id="more" data-link="{{ $post->nextPageUrl() }}" style="display: {{ $post->nextPageUrl() == null ? 'none;' : 'inline;' }}">See more</button>
</div>

<!-- Ads1 -->
{!! randomAdsFirst() !!}
<!-- ads1 akhir -->

<!-- Line -->
<div class="container pt-2">
    <hr>
</div>
<!-- Akhir Line -->
@endsection

@section('custom-script')
<script>
    $(document).ready(function() {
        $("div.detail-post").find('iframe').parent().css({"display":"flex","justify-content":"center"});
    });

    $('#more').on('click', function(){
        ajaxGet($(this).data('link'), null, '#more', 'See More')
        .done(function(res){
            var data = res.data;
            $('#list').append(data.view);
            $('#more').data('link', data.link);

            if(data.link == null) {
                $('#more').hide();
            }
        });
    });
</script>
@endsection