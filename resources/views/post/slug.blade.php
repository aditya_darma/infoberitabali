@extends('layouts.frontend.app')

@section('title', $post->title)

@section('content')
    <!-- Garis -->

    <div class="container">
        <hr>
    </div>

    <!-- Akhir Garis -->

    <!-- Ads1 -->
    {!! randomAdsFirst() !!}
    <!-- ads1 akhir -->



    <!-- Breadcump -->
    <div class="container">
        <nav aria-label="breadcrumb ">
            <ol class="breadcrumb bg-transparent">
                <li class="breadcrumb-item"><a href="{{ route('index') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route($post->type_post.'.index') }}">{{ typePost()[$post->type_post] }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $post->categoryPost->name }}</li>
            </ol>
        </nav>
    </div>
    <!-- Akhir breadcump -->

    <!-- Content -->

    <div class="container">
        <div class="img-post text-center">
            <img class="img-fluid" src="{{ pathImagePost($post->image) }}" alt="">
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-10">
                <div class="detail-post">
                    <h5 class="title-post">{{ $post->title }}</h5>
                    <div class="row">
                        <div class="col-6">
                            <p class="Postinger">Post By : {{ $post->user->name }}</p>
                        </div>
                        <div class="col-6">
                            <p class="Postinger" style="text-align: right;">{{ $post->created_at->format('d | m | Y') }}</p>
                        </div>
                    </div>
                    {!! replaceForViewAds($post->content) !!}
                    <div class="row align-items-center">
                        <div class="col view-custom">
                            {{ $post->countCommentActive() }} Comment
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Akhir Content -->

    <div class="container">
        <div class="title-custom pt-4">
            <div class="row d-flex justify-content-center">
                <h1>Share Button</h1>
            </div>
        </div>
    </div>


    <!-- Share -->

    <div class="text-center custom-icon pt-2">
        <a href="https://api.whatsapp.com/send?text={{ route($post->type_post.'.slug', ['slug' => $post->slug]) }}" target="_blank">
            <i class="fab fa-whatsapp-square"></i>
        </a>
        <a href="https://twitter.com/intent/tweet?text={{ route($post->type_post.'.slug', ['slug' => $post->slug]) }}" target="_blank">
            <i class="fab fa-twitter-square"></i>
        </a>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ route($post->type_post.'.slug', ['slug' => $post->slug]) }}" target="_blank">
            <i class="fab fa-facebook-square"></i>
        </a>
    </div>

    <!-- Akhir Share -->


    <!-- Komentar -->

    <div class="container mt-5 mb-5">
        <div class="d-flex justify-content-center row">
            <div class="col-md-8">
                @if (isLogin() && isVisitor())
                <form id="post-comment" action="{{ route('comment') }}">
                    <div class="d-flex flex-row align-items-center add-comment p-2 bg-white rounded">
                        <div class="avatar-user">{{ substr(auth()->user()->name,0,1) }}</div>
                        <input type="hidden" name="id" id="id" value="{{ $post->id }}">
                        <input type="text" class="form-control border-0 no-box-shadow ml-1" name="comment" placeholder="Leave a constructive comment...">
                        <button class="btn btn-sm px-3" style="background-color: #9e0a10; color: white;" type="submit" id="btn-comment">Send</button>
                    </div>
                </form>
                @endif
                <div id="comment"></div>
                <div class="text-center pt-4 pb-4">
                    <a href="javascript:void(0)" style="color: #9e0a10; text-decoration: none; display:none;" id="btn-see-more">See More</a>
                </div>
                <div class="p-3 bg-white mt-3 rounded text-center">
                    <h5>Click Signup or login to Comment</h5>
                    <button class="btn btn-sm px-3" type="button" style="background-color: #b87c7e; color: rgb(58, 11, 11);" onclick="openRegisterModal();">Signup</button>
                    <button class="btn btn-sm px-3" type="button" style="background-color: #9e0a10; color: white;" onclick="openLoginModal();">Login</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Akhir Komentar -->

    <!-- Garis -->

    <div class="container">
        <hr>
    </div>

    <!-- Akhir Garis -->
@endsection

@section('custom-script')
<script>
    let id = '{{ $post->id }}';
    let url = '{{ route("comment-post", ":id") }}';
    let next = '';
    $(document).ready(function() {
        $("div.detail-post").find('iframe').parent().css({"display":"flex","justify-content":"center"});

        getComment();
   });

   $('#post-comment').submit(function(e){
        e.preventDefault();
        const url = $(this).attr('action');
        const data = new FormData(this);

        ajaxPost(url, data, '#post-comment', '#btn-comment', 'Send')
        .done(function(res){
            if (res.status) {
                swal({
                    title: 'Success',
                    text: res.message,
                    icon: 'success'
                }).then(function(){
                    location.reload();
                });
            } else {
                swal({
                    title: 'Fail!',
                    text: res.message,
                    icon: 'error'
                });
            }
        })
    });

    $('#btn-see-more').on('click', function(){
        getComment();
    });

    function getComment()
    {
        let url = '{{ route("comment-post", ":id") }}';
            url = url.replace(':id',id);

        if (next) {
            url = next;
        }

        ajaxGet(url, {}, '#btn-see-more', 'See More')
        .done(function(res){
            $('#comment').append(res.data.view);
            if (res.data.link) {
                next = res.data.link
                $('#btn-see-more').show();
            } else {
                $('#btn-see-more').hide();
            }
        });
    }
</script>
@endsection
