@extends('layouts.backend.app')

@section('title', 'Komentar')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Komentar</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Komentar</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row mt-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Data Semua Komentar</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" id="table" width="100%">
                                <thead>                                 
                                    <tr>
                                        <th>No</th>
                                        <th>Komentar</th>
                                        <th>Judul Post</th>
                                        <th>Waktu</th>
                                        <th>Pengguna</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('custom-script')
<script src="{{ asset('assets/backend/js/comment.js') }}"></script>
<script>
    $(document).ready(function(){
        //  load table
        const url = "{{ route('admin.comment.datatable') }}";
        new table(url);
    });

    // show
    $('#table').on('click','.show-comment', function(){
        let id = $(this).data('id');
        let url = '{{ route("admin.comment.show",":id") }}';
            url = url.replace(':id',id); 
        new show(url);
    });

    // hide
    $('#table').on('click','.hide-comment', function(){
        let id = $(this).data('id');
        let url = '{{ route("admin.comment.hide",":id") }}';
            url = url.replace(':id',id); 
        new hide(url);
    });

    // delete
    $('#table').on('click','.delete-comment', function(){
        swal({
            title: 'Apakah anda yakin?',
            text: 'Data akan dihapus, tidak bisa dikembalikan lagi!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                let id = $(this).data('id');
                let url = '{{ route("admin.comment.delete",":id") }}';
                    url = url.replace(':id',id); 
                new destroy(url);
            }
        });
    });
</script>
@endsection