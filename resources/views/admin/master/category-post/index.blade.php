@extends('layouts.backend.app')

@section('title', 'Kategori Post')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Kategori Post</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="#">Data Master</a></div>
            <div class="breadcrumb-item">Kategori Post</div>
        </div>
    </div>

    <div class="section-body">
        <div class="card">
            <div class="card-header">
                <h5>Data Kategori Post</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 pb-1">
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm" id="add">Tambah Data</a>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped" id="table" width="100%">
                        <thead>                                 
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@include('admin.master.category-post.modal')
@endsection

@section('custom-script')
<script src="{{ asset('assets/backend/js/master/category-post.js') }}"></script>
<script>
    $(document).ready(function(){
        //  load table
        const url = "{{ route('admin.master.category-post.datatable') }}";
        new table(url);
    });

    // create
    $('#add').on('click', function(){
        $('.modal-title').text('Tambah Kategori');
        $('#form-modal').attr('action', "{{ route('admin.master.category-post.store') }}");
        $('#modal-form').modal('show');
    });
    
    // submit
    $('#form-modal').submit(function(e){
        e.preventDefault();
        const url = $(this).attr('action');
        const data = new FormData(this);
        new submit(url, data);
    });

    // edit
    $('#table').on('click','.edit', function(){
        let id = $(this).data('id');
        let url = '{{ route("admin.master.category-post.edit",":id") }}';
            url = url.replace(':id',id);

        let action = '{{ route("admin.master.category-post.update",":id") }}';
            action = action.replace(':id',id);
        $('#form-modal').attr('action', action);
        $('.modal-title').text('Edit Kategori');

        new edit(url);
    });

    // delete
    $('#table').on('click','.delete', function(){
        swal({
            title: 'Apakah anda yakin?',
            text: 'Data akan dihapus, tidak bisa dikembalikan lagi!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                let id = $(this).data('id');
                let url = '{{ route("admin.master.category-post.delete",":id") }}';
                    url = url.replace(':id',id); 
                new destroy(url);
            }
        });
    });
</script>
@endsection