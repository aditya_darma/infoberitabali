@extends('layouts.backend.app')

@section('title', 'Setup Footer')

@section('library-style')
<link rel="stylesheet" href="{{ asset('assets/backend/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Setup</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Setup</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row mt-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Setup Footer</h4>
                    </div>
                    <div class="card-body">
                        <form id="form-data" action="{{ route('admin.setup.footer-update') }}" onsubmit="return false;">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"><h6>Sosmed</h6></label>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Facebook</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[sosmed_facebook]" value="{{ $setup['sosmed_facebook'] }}" placeholder="Link sosmed Facebook">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Instagram</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[sosmed_instagram]" value="{{ $setup['sosmed_instagram'] }}" placeholder="Link sosmed Instagram">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Youtube</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[sosmed_youtube]" value="{{ $setup['sosmed_youtube'] }}" placeholder="Link sosmed Youtube">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tiktok</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[sosmed_tiktok]" value="{{ $setup['sosmed_tiktok'] }}" placeholder="Link sosmed Tiktok">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Twitter</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[sosmed_twitter]" value="{{ $setup['sosmed_twitter'] }}" placeholder="Link sosmed Twitter">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"><h6>Kontak</h6></label>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Email</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[contact_email]" value="{{ $setup['contact_email'] }}" placeholder="Alamat Email">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Phone</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[contact_phone]" value="{{ $setup['contact_phone'] }}" placeholder="No Telp">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Whatsapp</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[contact_whatsapp]" value="{{ $setup['contact_whatsapp'] }}" placeholder="No Whatsapp">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Line</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[contact_line]" value="{{ $setup['contact_line'] }}" placeholder="Nama Line">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"><h6>About</h6></label>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Locations</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[about_locations]" value="{{ $setup['about_locations'] }}" placeholder="About Locations">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Privacy</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[about_privacy]" value="{{ $setup['about_privacy'] }}" placeholder="About Privacy">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Terms</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[about_terms]" value="{{ $setup['about_terms'] }}" placeholder="About Terms">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Sitemap</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="setup[about_sitemap]" value="{{ $setup['about_sitemap'] }}" placeholder="About Sitemap">
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('custom-script')
<script src="{{ asset('assets/backend/js/setup.js') }}"></script>
<script>
    $('#form-data').submit(function(e){
        e.preventDefault();
        const url = $(this).attr('action');
        const data = new FormData(this);
        new submit(url, data);
    });
</script>
@endsection
