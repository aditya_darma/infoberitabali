@extends('layouts.backend.app')

@section('title', 'Setup About')

@section('library-style')
<link rel="stylesheet" href="{{ asset('assets/backend/modules/summernote/summernote-bs4.css') }}">
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Setup</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Setup</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row mt-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Setup About</h4>
                    </div>
                    <div class="card-body">
                        <form id="form-data" action="{{ route('admin.setup.about-us-update') }}" onsubmit="return false;">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                                <div class="col-sm-12 col-md-7">
                                    <textarea class="summernote" name="content">{{ $setup->value ?? null }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('library-script')
<script src="{{ asset('assets/backend/modules/summernote/summernote-bs4.js') }}"></script>
@endsection

@section('custom-script')
<script src="{{ asset('assets/backend/js/setup.js') }}"></script>
<script>
    $('#form-data').submit(function(e){
        e.preventDefault();
        const url = $(this).attr('action');
        const data = new FormData(this);
        new submit(url, data);
    });
</script>
@endsection
