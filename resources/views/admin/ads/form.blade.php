@extends('layouts.backend.app')

@section('title', $title)

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Iklan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Iklan</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row mt-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Tambah Iklan</h4>
                    </div>
                    <div class="card-body">
                        <form id="form-data" action="{{ $url }}" onsubmit="return false;">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Nama</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Iklan" value="{{ $ads->name ?? null }}">
                                    <div class="error-name form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Ukuran</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" name="size_type">
                                        <option value="">- Pilih Ukuran -</option>
                                        @foreach ($type as $key => $value)
                                        <option value="{{ $key }}" {{ (($ads->size_type ?? null) == $key) ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                    <div class="error-size_type form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Durasi Tampil</label>
                                <div class="col-sm-12 col-md-3">
                                    <input type="text" class="form-control datepicker" name="date_start" placeholder="Tanggal Mulai Iklan" value="{{ isset($ads) ? $ads->date_start->format('d-m-Y') : null }}">
                                    <div class="error-date_start form-error text-danger fs-12"></div><br>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <input type="text" class="form-control datepicker" name="date_end" placeholder="Tanggal Berakhir Iklan" value="{{ isset($ads) ? $ads->date_end->format('d-m-Y') : null }}">
                                    <div class="error-date_end form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Link</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="link" placeholder="Link Iklan" value="{{ $ads->link ?? null }}">
                                    <div class="error-link form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" name="status">
                                        <option value="">- Pilih Status -</option>
                                        <option value="0" {{ (($ads->active ?? null) == 0) ? 'selected' : '' }}>Tidak Aktif</option>
                                        <option value="1" {{ (($ads->active ?? null) == 1) ? 'selected' : '' }}>Aktif</option>
                                    </select>
                                    <div class="error-status form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Photo</label>
                                <div class="col-sm-12 col-md-7">
                                    <img id="img-input-read" src="{{ ($ads->image ?? null) ? asset('content/photo/ads/'.$ads->image) : null }}" class="img-fluid mb-2"><br>
                                    <input type="file" id="img-input" name="image" accept="image/png, image/gif, image/jpeg">
                                    <div class="error-image form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('custom-script')
<script src="{{ asset('assets/backend/js/ads.js') }}"></script>
<script>
    $("#img-input").change(function () {
        readURL(this, $('#img-input-read'));
    });

    $('#form-data').submit(function(e){
        e.preventDefault();
        const url = $(this).attr('action');
        const data = new FormData(this);
        const redirect = '{{ route("admin.ads.index") }}';
        new submit(url, data, redirect);
    });
</script>
@endsection
