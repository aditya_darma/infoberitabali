@extends('layouts.backend.app')

@section('title', 'Iklan')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Iklan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Iklan</div>
        </div>
    </div>

    <div class="section-body">
        <div class="card">
            <div class="card-header">
                <h5>Data Iklan</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 pb-1">
                        <a href="{{ route('admin.ads.create') }}" class="btn btn-primary btn-sm">Tambah Data</a>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped" id="table" width="100%">
                        <thead>                                 
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Link</th>
                                <th>Ukuran</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('custom-script')
<script src="{{ asset('assets/backend/js/ads.js') }}"></script>
<script>
    $(document).ready(function(){
        //  load table
        const url = "{{ route('admin.ads.datatable') }}";
        new table(url);
    });

    // delete
    $('#table').on('click','.delete', function(){
        swal({
            title: 'Apakah anda yakin?',
            text: 'Data akan dihapus, tidak bisa dikembalikan lagi!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                let id = $(this).data('id');
                let url = '{{ route("admin.ads.delete",":id") }}';
                    url = url.replace(':id',id); 
                new destroy(url);
            }
        });
    });
</script>
@endsection