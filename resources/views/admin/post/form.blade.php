@extends('layouts.backend.app')

@section('title', $title)

@section('library-style')
<link rel="stylesheet" href="{{ asset('assets/backend/modules/summernote/summernote-bs4.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/modules/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/modules/jquery-selectric/selectric.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Artikel</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Artikel</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row mt-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Tambah Artikel</h4>
                    </div>
                    <div class="card-body">
                        <form id="form-data" action="{{ $url }}" onsubmit="return false;">
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Judul</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" name="title" placeholder="Judul Artikel" value="{{ $post->title ?? null }}">
                                    <div class="error-title form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tipe</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" name="type">
                                        <option value="">- Pilih Tipe -</option>
                                        @foreach ($type as $key => $value)
                                        <option value="{{ $key }}" {{ (($post->type_post ?? null) == $key) ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                    <div class="error-type form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kategori</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control select2" name="category">
                                        <option value="">- Pilih Kategori -</option>
                                        @foreach ($category as $item)
                                        <option value="{{ $item->id }}" {{ (($post->category_post_id ?? null) == $item->id) ? 'selected' : '' }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="error-category form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                                <div class="col-sm-12 col-md-7">
                                    <textarea class="summernote" name="content">{{ $post->content ?? null }}</textarea>
                                    <div class="error-image form-error fs-12"><b>Note:</b> Gunakan kata {iklan_*} untuk mengisi iklan (* adalah nomor iklan yang dimulai dari 1)</div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Photo</label>
                                <div class="col-sm-12 col-md-7">
                                    <img id="img-input-read" src="{{ ($post->image ?? null) ? pathImagePost($post->image) : null }}" class="img-fluid mb-2"><br>
                                    <input type="file" id="img-input" name="image" accept="image/png, image/gif, image/jpeg">
                                    <div class="error-image form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Keyword</label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control inputtags" name="keyword" value="{{ $post->keyword ?? null }}">
                                    <div class="error-keyword form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Highlight</label>
                                <div class="col-sm-12 col-md-7">
                                    <label class="custom-switch mt-2">
                                        <input type="checkbox" name="highland" class="custom-switch-input" {{ ($post->highland ?? null) ? "checked" : null }}>
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status</label>
                                <div class="col-sm-12 col-md-7">
                                    <select class="form-control selectric" name="status">
                                        <option value="">- Pilih Status -</option>
                                        @foreach ($status as $key => $value)
                                        <option value="{{ $key }}" {{ (($post->status ?? null) == $key) ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                    <div class="error-status form-error text-danger fs-12"></div>
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                <div class="col-sm-12 col-md-7">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('library-script')
<script src="{{ asset('assets/backend/modules/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('assets/backend/modules/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/backend/modules/jquery-selectric/jquery.selectric.min.js') }}"></script>
<script src="{{ asset('assets/backend/modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
@endsection

@section('custom-script')
<script src="{{ asset('assets/backend/js/post.js') }}"></script>
<script>
    $(document).ready(function(){
        $(".inputtags").tagsinput('items');
    });
    $("#img-input").change(function () {
        readURL(this, $('#img-input-read'));
    });

    $('#form-data').submit(function(e){
        e.preventDefault();
        const url = $(this).attr('action');
        const data = new FormData(this);
        const redirect = '{{ route("admin.post.index") }}';
        new submit(url, data, redirect);
    });
</script>
@endsection
