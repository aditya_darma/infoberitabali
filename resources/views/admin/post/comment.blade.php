@extends('layouts.backend.app')

@section('title', 'Komentar')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Komentar</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Komentar</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row mt-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Data Komentar Post {{ $post->title }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 pb-1">
                                <a href="javascript:void(0)" class="btn btn-primary btn-sm" id="add">Tambah Komentar</a>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-striped" id="table" width="100%">
                                <thead>                                 
                                    <tr>
                                        <th>No</th>
                                        <th>Komentar</th>
                                        <th>Waktu</th>
                                        <th>Pengguna</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('admin.post.modal-comment')
@endsection

@section('custom-script')
<script src="{{ asset('assets/backend/js/post.js') }}"></script>
<script>
    $(document).ready(function(){
        //  load table
        let id = '{{ $post->id }}';
        let url = '{{ route("admin.post.comment.datatable",":id") }}';
            url = url.replace(':id',id);
        new tableComment(url);
    });

    // show
    $('#table').on('click','.show-comment', function(){
        let id = $(this).data('id');
        let url = '{{ route("admin.comment.show",":id") }}';
            url = url.replace(':id',id); 
        new show(url);
    });

    // hide
    $('#table').on('click','.hide-comment', function(){
        let id = $(this).data('id');
        let url = '{{ route("admin.comment.hide",":id") }}';
            url = url.replace(':id',id); 
        new hide(url);
    });

    // delete
    $('#table').on('click','.delete-comment', function(){
        swal({
            title: 'Apakah anda yakin?',
            text: 'Data akan dihapus, tidak bisa dikembalikan lagi!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                let id = $(this).data('id');
                let url = '{{ route("admin.comment.delete",":id") }}';
                    url = url.replace(':id',id); 
                new destroy(url);
            }
        });
    });

    // add comment
    $('#add').on('click', function(){
        let id = '{{ $post->id }}';
        let url = '{{ route("admin.post.comment.store",":id") }}';
            url = url.replace(':id',id); 
        $('#form-comment').attr('action', url);
        $('.modal-title').text('Tambah Komentar');
        $('#modal-comment').modal('show');
    });

    // submit comment
    $('#form-comment').submit(function(e){
        e.preventDefault();
        const url = $(this).attr('action');
        const data = new FormData(this);
        new submitComment(url, data);
    });

    // edit
    $('#table').on('click','.edit-comment', function(){
        let id_post = '{{ $post->id }}';
        let id_comment = $(this).data('id');
        let url = '{{ route("admin.post.comment.edit", [":id",":comment"]) }}';
            url = url.replace(':id',id_post);
            url = url.replace(':comment',id_comment);
        
        let action = '{{ route("admin.post.comment.update", [":id",":comment"]) }}';
            action = action.replace(':id',id_post);
            action = action.replace(':comment',id_comment);

        $('.modal-title').text('Edit Komentar');
        new editComment(url,action);
    });
</script>
@endsection