@foreach ($post as $key => $item)
<div class="col-md-{{ $column[$key] }}">
    <a href="{{ route($item->type_post.'.slug',['slug' => $item->slug]) }}" class="custom-link">
        <div class="card custom-card mb-4">
            <div class="tags">{{ $item->categoryPost->name }}</div>
            <img class="card-img-top img-card img-fluid" src="{{ pathImagePost($item->image) }}" alt="Card image cap">
            <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <p class="Postinger">Post By : {{ $item->user->name }}</p>
                    </div>
                    <div class="col-6">
                        <p class="Postinger" style="text-align: right;">{{ $item->created_at->format('d | m | Y') }}</p>
                    </div>
                </div>
                <h5 class="card-title">{{ strlen($item->title) > 50 ? substr($item->title,0,50).'...' : substr($item->title,0,50) }}</h5>
                <h5 class="card-text">{{ substr(strip_tags($item->content),0,130) }}... <b>Selengkapnya</b></h5>
                <hr>
                <div class="row align-items-center">
                    <div class="col view-custom">
                        {{ $item->countCommentActive() }} Comment
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
@endforeach
