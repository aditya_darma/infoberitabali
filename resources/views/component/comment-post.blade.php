@foreach ($comment as $item)
<div class="p-3 bg-white mt-3 rounded">
    <div class="d-flex justify-content-between">
        <div class="d-flex flex-row user">
            <div class="avatar">{{ substr($item->user->name,0,1) }}</div>
            <div class="d-flex flex-column ml-2"><span class="font-weight-bold">{{ $item->user->name }}</span><span class="day">{{ $item->created_at->format('d F Y H:i:s') }}</span></div>
        </div>
    </div>
    <div class="comment-text text-justify mt-2">
        {{ $item->comment }}
    </div>
</div>
@endforeach