@extends('layouts.frontend.app')

@section('title', 'About')

@section('content')
    <!-- Garis -->

    <div class="container">
        <hr>
    </div>

    <!-- Akhir Garis -->

    <!-- About US -->
    <div class="container">
        <div class="title-custom text-center pt-4">
            <h1>About US</h1>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-8 pt-4 pb-4">
                <div class="card-text" style="line-height: 10rm; text-align: justify;">
                    {!! $setup->value !!}
                </div>
            </div>
        </div>
    </div>
    <!-- Akhir About US -->


    <!-- Garis -->

    <div class="container">
        <hr>
    </div>

    <!-- Akhir Garis -->
@endsection