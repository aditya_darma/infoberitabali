@extends('layouts.frontend.app')

@section('title', 'Home')

@section('content')
    @if (Session::has('success'))
        <div class="row justify-content-center">
            <div class="col-md-2">
                <div class="alert alert-success text-center">
                    {!! Session::get('success') !!}
                </div>
            </div>
        </div>
    @endif
    @if (Session::has('error'))
        <div class="row justify-content-center">
            <div class="col-md-2">
                <div class="alert alert-danger text-center">
                    {!! Session::get('error') !!}
                </div>
            </div>
        </div>
    @endif
    <!-- Title search -->
    <div class="container  custom-title-search" style="padding-top: 5%;">
        <div class="row d-flex justify-content-center text-center">
            <div class="col-md-7">
                <h1 class="text-slider-items">Selamat Datang, di website, Info Berita Bali</h1>
                <strong class="text-slider"></strong>
                <p class="pt-4">Silahkan masukan kata kunci pencarian anda pada kolom di bawah ini, atau bisa langsung masuk ke menu navigasi di atas untuk mencari berita artikel dan lainnya.</p>
            </div>
        </div>
    </div>
    <!-- Title akhir search -->


    <!-- Search -->
    <div class="container pt-4 pb-4">
        <form action="{{ route('search') }}" method="GET">
            <div class="row d-flex justify-content-center">
                <div class="col-md-6">
                    <div class="mb-1">
                        <div class="d-flex w-100 div-input">
                            <img src="{{ asset('assets/icon/search.svg') }}" style="width: 32px;" alt="">
                            <input class="input-field border-0" name="search" placeholder="Search" autocomplete="on" required />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pt-4">
                <div class="col text-center">
                    <button type="submit" class="btn btn-danger" id="" style="width: 100px; padding: 10px 0px 10px 0px; background-color: #9e0a10; border-radius: 20px;">Search</button>
                </div>
            </div>
        </form>
    </div>
    <!-- Akhir Search -->

    <!-- Ads1 -->
    {!! randomAdsFirst() !!}
    <!-- ads1 akhir -->

    <!-- Garis -->

    <div class="container">
        <hr>
    </div>

    <!-- Akhir Garis -->


    <!-- Section 1 -->
    <div class="container" style="margin-top: 2%;">
        <div class="row">
            <div class="col-md-6">
                <div class="title-custom pb-4">
                    <p>
                        <h1>Highlight</h1>
                        <div class="line-custom"></div>
                    </p>
                </div>
                <!-- Card berita 1-->
                @if ($highland)
                <a href="{{ route($highland->type_post.'.slug',['slug' => $highland->slug]) }}" class="custom-link">
                    <div class="card custom-card mb-4">
                        <div class="tags">{{ $highland->categoryPost->name }}</div>
                        <img class="card-img-top img-card img-fluid" src="{{ pathImagePost($highland->image) }}" alt="Card image cap">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-6">
                                    <p class="Postinger">Post By : {{ $highland->user->name }}</p>
                                </div>
                                <div class="col-6">
                                    <p class="Postinger" style="text-align: right;">{{ $highland->created_at->format('d | m | Y') }}</p>
                                </div>
                            </div>
                            <h5 class="card-title">{{ $highland->title }}</h5>
                            <h5 class="card-text">{{ substr(strip_tags($highland->content),0,200) }}... <b>Selengkapnya</b></h5>

                            <hr>
                            <div class="row align-items-center">
                                <div class="col view-custom">
                                    {{ $highland->countCommentActive() }} Comment
                                </div>
                            </div>

                        </div>
                    </div>
                </a>
                @endif
                <!-- Card berita 1 akhir -->

                <!-- Ads2 -->
                {!! randomAdsSecond() !!}
                <!-- ads2 akhir -->

                <!-- Garis -->
                <hr>
                <!-- Akhir Garis -->


                <div class="row">
                    <div class="col-md-12">
                        <div class="title-custom pb-4">
                            <p>
                                <h1>Latest Article</h1>
                                <div class="line-custom"></div>
                            </p>
                            <div class="row pt-3 latest-artc">
                                @foreach ($latests_article as $item)
                                <div class="col-md-12 pt-3">
                                    <a href="{{ route($item->type_post.'.slug',['slug' => $item->slug]) }}">
                                        <div class="row card-custom">
                                            <div class="col-md-4">
                                                <img class="img-fluid Latest-article" src="{{ pathImagePost($item->image) }}" alt="Card image cap">
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="card-title cut-text ">{{ $item->title }}</h5>
                                                <h5 class="card-text">{{ substr(strip_tags($item->content),0,65) }}... <b>Selengkapnya</b>
                                                </h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                                @if ($latests_article->count() >= 4)
                                <div class="col-md-12 text-center pt-4">
                                    <a href="{{ route('article.index') }}" class="btn btn-custom my-2 my-sm-0 ml-sm-4 text-white">See more</a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-md-6">
                <div class="title-custom pb-4">
                    <p>
                        <h1>News</h1>
                        <div class="line-custom"></div>
                    </p>
                </div>
                <div class="row">
                    <!-- Card News -->
                    @foreach ($news as $item)
                    <div class="col-md-6">
                        <a href="{{ route($item->type_post.'.slug',['slug' => $item->slug]) }}" class="custom-link">
                            <div class="card custom-card-2 mb-4">
                                <div class="tags">{{ $item->categoryPost->name }}</div>
                                <img class="card-img-top img-card2" src="{{ pathImagePost($item->image) }}" alt="Card image cap">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="Postinger">Post By : {{ $item->user->name }}</p>
                                        </div>
                                        <div class="col-6">
                                            <p class="Postinger" style="text-align: right;">{{ $item->created_at->format('d | m | Y') }}</p>
                                        </div>
                                    </div>
                                    <h5 class="card-title-2">{{ substr($item->title,0,50) }}{{ strlen($item->title) > 50 ? '...' : '' }}</h5>
                                    <h5 class="card-text-2">{{ substr(strip_tags($item->content),0,130) }}...<b>Selengkapnya</b></h5>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    <!-- Card News akhir -->

                    <!-- Card Popular Article -->
                    <div class="col-md-12">
                        <div class="title-custom pb-4">
                            <p>
                                <h1>POPULAR ARTICLE</h1>
                                <div class="line-custom"></div>
                            </p>
                            <div class="row">
                                @foreach ($populars_article as $key => $item)
                                <div class="col-12">
                                    <a href="{{ route($item->type_post.'.slug',['slug' => $item->slug]) }}" class="custom-link">
                                        <div class="card custom-card-2 mb-4">
                                            <div class="custom-number d-flex align-items-center justify-content-center">0{{ ++$key }}</div>
                                            <h5 class="card-title-2 cut-text pr-5">{{ $item->title }}</h5>
                                            <h5 class="card-text-2">{{ substr(strip_tags($item->content),0,130) }}...<b>Selengkapnya</b></h5>
                                            <div class="row">
                                                <div class="col-6">
                                                    <p class="Postinger">Post By : {{ $item->user->name }}</p>
                                                </div>
                                                <div class="col-6">
                                                    <p class="Postinger" style="text-align: right;">{{ $item->created_at->format('d | m | Y') }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                                @if ($populars_article->count() >= 4)
                                <div class="col-md-12 text-center">
                                    <a href="{{ route('article.popular') }}" class="btn btn-custom my-2 my-sm-0 ml-sm-4 text-white">See more</a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Card Popular Article akhir -->
                </div>
            </div>
        </div>
        <!-- Section1 akhir -->

        <!-- Garis -->
        <hr>
        <!-- Akhir Garis -->

        <!-- Ads1 -->
        {!! randomAdsFirst() !!}
        <!-- ads1 akhir -->
    </div>
@endsection

@section('custom-script')
    <script>
        if ($(".text-slider").length == 1) {
            var typed_strings = $(".text-slider-items").text();
            var typed = new Typed(".text-slider", {
                strings: typed_strings.split(", "),
                typeSpeed: 50,
                loop: true,
                backDelay: 900,
                backSpeed: 30,
            });
        }
    </script>
@endsection
