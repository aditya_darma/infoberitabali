@extends('layouts.frontend.app')

@section('title', 'Contact')

@section('content')
    <!-- Garis -->

    <div class="container">
        <hr>
    </div>

    <!-- Akhir Garis -->

    <!-- About US -->
    <div class="container">
        <div class="title-custom text-center pt-4">
            <h1>Contact US</h1>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-8 pt-4 pb-4">
                <div class="card-text" style="line-height: 10rm; text-align: justify;">
                    <div class="card custom-card p-5">
                        <form id="form-contact" action="{{ route('contact-store') }}" onsubmit="return false;">
                            <div class="row">
                                <div class="col-xl-6 col-md-6">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input class="form-control" type="text" name="name" id="name" placeholder="Your Name">
                                        <div class="error-name form-error text-danger fs-12"></div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input class="form-control" type="text" name="email" id="email" placeholder="Your Email">
                                        <div class="error-email form-error text-danger fs-12"></div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-md-12">
                                    <div class="form-group">
                                        <label for="subject">Subject</label>
                                        <input class="form-control" type="text" name="subject" id="subject" placeholder="Your Subject">
                                        <div class="error-subject form-error text-danger fs-12"></div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-md-12">
                                    <div class="form-group">
                                        <label for="name">Message</label>
                                        <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="Your Message"></textarea>
                                        <div class="error-message form-error text-danger fs-12"></div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-custom" id="btn-contact">Submit</button>
                                    </div>
                                </div>
                             </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Akhir About US -->


    <!-- Garis -->

    <div class="container">
        <hr>
    </div>

    <!-- Akhir Garis -->
@endsection

@section('custom-script')
<script>
    $('#form-contact').submit(function(e){
        e.preventDefault();
        const url = $(this).attr('action');
        const data = new FormData(this);
        
        ajaxPost(url, data, '#form-contact', '#btn-contact', 'Submit')
        .done(function(res){
            if (res.status) {
                swal({
                    title: 'Success',
                    text: res.message,
                    icon: 'success'
                }).then(function(){
                    location.reload();
                });
            } else {
                swal({
                    title: 'Fail!',
                    text: res.message,
                    icon: 'error'
                });
            }
        })
    });
</script>
@endsection