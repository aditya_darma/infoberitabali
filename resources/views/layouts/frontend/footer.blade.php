<div class="container">
    <footer class="pt-4 my-md-5 pt-md-5">
        <div class="row">
            <div class="col-12 col-md foother-text">
                <img class="mb-2" src="{{ asset('assets/Logo.png') }}" alt="" width="100">
            </div>
            <div class="col-6 col-md foother-text">
                <h5>Contact</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="mailto:{{ setup('contact_email') }}">Email</a></li>
                    <li><a class="text-muted" href="tel:{{ setup('contact_phone') }}">Phone</a></li>
                    <li><a class="text-muted" href="https://api.whatsapp.com/send?phone={{ setup('contact_whatsapp') }}">Whatsapp</a></li>
                    <li><a class="text-muted" href="http://line.me/ti/p/~{{ setup('contact_line') }}">Line</a></li>
                </ul>
            </div>
            <div class="col-6 col-md foother-text">
                <h5>Social Media</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="{{ setup('sosmed_facebook') }}">Facebook</a></li>
                    <li><a class="text-muted" href="{{ setup('sosmed_instagram') }}">Instagram</a></li>
                    <li><a class="text-muted" href="{{ setup('sosmed_youtube') }}">Youtube</a></li>
                    <li><a class="text-muted" href="{{ setup('sosmed_tiktok') }}">Tiktok</a></li>
                    <li><a class="text-muted" href="{{ setup('sosmed_twitter') }}">Twitter</a></li>
                </ul>
            </div>
            <div class="col-6 col-md foother-text">
                <h5>About</h5>
                <ul class="list-unstyled text-small">
                    <li><a class="text-muted" href="{{ setup('about_locations') }}">Locations</a></li>
                    <li><a class="text-muted" href="{{ setup('about_privacy') }}">Privacy</a></li>
                    <li><a class="text-muted" href="{{ setup('about_terms') }}">Terms</a></li>
                    <li><a class="text-muted" href="{{ setup('about_sitemap') }}">Sitemap</a></li>
                </ul>
            </div>
        </div>

    </footer>
    <div class="text-center">
        <div style="font-size: 16px; color: gray;">
            <p>© {{ date('Y') }} <a style="text-decoration: none; color: gray; " href="{{ route('index') }}" target="_blank">Info Berita Bali</a> • All rights reserved • Love from Bali.</p>
        </div>
    </div>
</div>
