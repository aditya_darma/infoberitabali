<div class="modal fade login" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered login animated" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Login with</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="box">
                    <div class="content">
                        <div class="form loginBox">
                            <form id="form-login" action="{{ route('auth.login') }}">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="email" placeholder="Email">
                                    <div class="error-email form-error text-danger fs-12"></div>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" name="password" placeholder="Password">
                                    <div class="error-password form-error text-danger fs-12"></div>
                                </div>
                                <button type="submit" class="btn btn-default btn-login" id="btn-login">Login</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <div class="content registerBox" style="display:none;">
                        <div class="form">
                            <form id="form-register" action="{{ route('auth.register') }}">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="name" placeholder="Nama">
                                    <div class="error-name form-error text-danger fs-12"></div>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="email" placeholder="Email">
                                    <div class="error-email form-error text-danger fs-12"></div>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" name="password" placeholder="Password">
                                    <div class="error-password form-error text-danger fs-12"></div>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" name="password_confirmation" placeholder="Ulangi Password">
                                    <div class="error-password_confirmation form-error text-danger fs-12"></div>
                                </div>
                                <button type="submit" class="btn btn-default btn-register" id="btn-register">Daftar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="forgot login-footer">
                    <span>Looking to
                         <a href="javascript: showRegisterForm();" style="color:#9e0a10;">create an account</a>
                    ?</span>
                </div>
                <div class="forgot register-footer" style="display:none">
                     <span>Already have an account?</span>
                     <a href="javascript: showLoginForm();" style="color:#9e0a10;">Login</a>
                </div>
            </div>
        </div>
    </div>
</div>