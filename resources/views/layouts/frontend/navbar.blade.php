<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light pt-md-4">
        <a href="{{ route('index') }}">
            <img src="{{ asset('assets/Logo.png') }}" width="100px" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('index') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('news.index') }}">News</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('highland') }}">Highlight</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Category
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        @foreach (listCategoryActive() as $key => $item)
                        <a class="dropdown-item" href="{{ route('category', ['slug' => $key]) }}">{{ $item }}</a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contact') }}">Contact</a>
                </li>
                @if (isLogin() && isVisitor())
                <ul class="navbar-nav" style="margin-top: -10px;">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset('assets/avatars/user.jpg') }}" width="33" height="33" class="rounded-circle">
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ route('auth.logout') }}">Log Out</a>
                        </div>
                    </li>
                </ul>
                @else
                <button type="button" class="btn btn-custom my-2 my-sm-0 ml-sm-4" data-toggle="modal" data-target="#loginModal">Login</button>
                @endif
            </ul>
        </div>
    </nav>
</div>