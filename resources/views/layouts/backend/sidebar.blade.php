<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('admin.dashboard') }}">Info Berita Bali</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('admin.dashboard') }}">IBB</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Main Menu</li>
            <li><a class="nav-link" href="{{ route('admin.dashboard') }}"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-database"></i><span>Data Master</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('admin.master.category-post.index') }}">Kategori Post</a></li>
                </ul>
            </li>
            <li><a class="nav-link" href="{{ route('admin.post.index') }}"><i class="fas fa-newspaper"></i> <span>Post</span></a></li>
            <li><a class="nav-link" href="{{ route('admin.comment.index') }}"><i class="fas fa-comments"></i> <span>Komentar</span></a></li>
            <li><a class="nav-link" href="{{ route('admin.ads.index') }}"><i class="fab fa-buysellads"></i> <span>Iklan</span></a></li>
            <li class="dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-cogs"></i><span>Pengaturan</span></a>
                <ul class="dropdown-menu">
                    <li><a class="nav-link" href="{{ route('admin.setup.about-us') }}">Tentang Kami</a></li>
                    <li><a class="nav-link" href="{{ route('admin.setup.footer') }}">Footer</a></li>
                </ul>
            </li>
            <li><a class="nav-link" href="{{ route('admin.logout') }}"><i class="fas fa-power-off"></i> <span>Logout</span></a></li>
        </ul>
    </aside>
</div>