<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('seed', function () {
    Artisan::call('db:seed');
});

// Website
Route::middleware('count.visitor')->group(function(){
    Route::get('/', [HomeController::class, 'index'])->name('index');
    Route::get('/about', [HomeController::class, 'about'])->name('about');
    Route::get('/contact', [HomeController::class, 'contact'])->name('contact');
    Route::post('/contact', [HomeController::class, 'contactStore'])->name('contact-store');
    Route::get('/search', [PostController::class, 'search'])->name('search');
    Route::get('/highlight', [PostController::class, 'highland'])->name('highland');
    Route::get('/category/{slug}', [PostController::class, 'category'])->name('category');
    Route::prefix('article')->name('article.')->group(function () {
        Route::get('/', [PostController::class, 'article'])->name('index');
        Route::get('/popular', [PostController::class, 'ArticlePopular'])->name('popular');
        Route::get('/{slug}', [PostController::class, 'slug'])->name('slug');
    });
    Route::prefix('news')->name('news.')->group(function () {
        Route::get('/', [PostController::class, 'news'])->name('index');
        Route::get('/{slug}', [PostController::class, 'slug'])->name('slug');
    });
    Route::post('/comment', [PostController::class, 'comment'])->name('comment');
    Route::get('/comment/{id}', [PostController::class, 'commentPost'])->name('comment-post');
    Route::prefix('auth')->name('auth.')->group(function () {
        Route::post('/register', [AuthController::class, 'register'])->name('register');
        Route::post('/login', [AuthController::class, 'login'])->name('login');
        Route::get('/verification', [AuthController::class, 'verification'])->name('verification');
        Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
    });
});





// Admin
Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('/login', [Admin\AuthController::class, 'login'])->name('login');
    Route::post('/login', [Admin\AuthController::class, 'loginPost'])->name('login.post');

    Route::group(['middleware' => 'admin'], function(){
        Route::get('/logout', [Admin\AuthController::class, 'logout'])->name('logout');
        Route::get('/dashboard', [Admin\DashboardController::class, 'index'])->name('dashboard');
        Route::get('/dashboard/visitor', [Admin\DashboardController::class, 'visitor'])->name('dashboard.visitor');

        Route::prefix('master')->name('master.')->group(function () {
            Route::prefix('category-post')->name('category-post.')->group(function () {
                Route::get('/', [Admin\Master\CategoryPostController::class, 'index'])->name('index');
                Route::get('/datatable', [Admin\Master\CategoryPostController::class, 'datatable'])->name('datatable');
                Route::post('/store', [Admin\Master\CategoryPostController::class, 'store'])->name('store');
                Route::get('/edit/{id}', [Admin\Master\CategoryPostController::class, 'edit'])->name('edit');
                Route::post('/update/{id}', [Admin\Master\CategoryPostController::class, 'update'])->name('update');
                Route::get('/delete/{id}', [Admin\Master\CategoryPostController::class, 'delete'])->name('delete');
            });
        });

        Route::prefix('post')->name('post.')->group(function () {
            Route::get('/', [Admin\PostController::class, 'index'])->name('index');
            Route::get('/datatable', [Admin\PostController::class, 'datatable'])->name('datatable');
            Route::get('/create', [Admin\PostController::class, 'create'])->name('create');
            Route::post('/store', [Admin\PostController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [Admin\PostController::class, 'edit'])->name('edit');
            Route::post('/update/{id}', [Admin\PostController::class, 'update'])->name('update');
            Route::get('/delete/{id}', [Admin\PostController::class, 'delete'])->name('delete');
            Route::prefix('comment')->name('comment.')->group(function () {
                Route::get('/{id}', [Admin\PostController::class, 'comment'])->name('index');
                Route::get('/{id}/datatable', [Admin\PostController::class, 'commentDatatable'])->name('datatable');
                Route::post('/{id}/store', [Admin\PostController::class, 'commentStore'])->name('store');
                Route::get('/{id}/edit/{comment}', [Admin\PostController::class, 'commentEdit'])->name('edit');
                Route::post('/{id}/update/{comment}', [Admin\PostController::class, 'commentUpdate'])->name('update');
            });
        });

        Route::prefix('comment')->name('comment.')->group(function () {
            Route::get('/', [Admin\CommentController::class, 'index'])->name('index');
            Route::get('/datatable', [Admin\CommentController::class, 'datatable'])->name('datatable');
            Route::get('/show/{id}', [Admin\CommentController::class, 'show'])->name('show');
            Route::get('/hide/{id}', [Admin\CommentController::class, 'hide'])->name('hide');
            Route::get('/delete/{id}', [Admin\CommentController::class, 'delete'])->name('delete');
        });

        Route::prefix('ads')->name('ads.')->group(function(){
            Route::get('/', [Admin\AdsController::class, 'index'])->name('index');
            Route::get('/datatable', [Admin\AdsController::class, 'datatable'])->name('datatable');
            Route::get('/create', [Admin\AdsController::class, 'create'])->name('create');
            Route::post('/store', [Admin\AdsController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [Admin\AdsController::class, 'edit'])->name('edit');
            Route::post('/update/{id}', [Admin\AdsController::class, 'update'])->name('update');
            Route::get('/delete/{id}', [Admin\AdsController::class, 'delete'])->name('delete');
        });

        Route::prefix('setup')->name('setup.')->group(function(){
            Route::get('/about-us', [Admin\SetupController::class, 'aboutUs'])->name('about-us');
            Route::post('/about-us', [Admin\SetupController::class, 'aboutUsUpdate'])->name('about-us-update');
            Route::get('/footer', [Admin\SetupController::class, 'footer'])->name('footer');
            Route::post('/footer', [Admin\SetupController::class, 'footerUpdate'])->name('footer-update');
        });
    });
});